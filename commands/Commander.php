<?php namespace Ikariam\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;

/**
 * Class Commander
 * @package App\Command
 */
class Commander extends Command
{
	/**
	 * {@inheritdoc}
	 */
	public function configure()
	{
		$this->setName('commander')
			 ->setDescription('Ikariam commander mode. (endless)');
	}



	/**
	 * {@inheritdoc}
	 */
	public function execute(InputInterface $input, OutputInterface $output)
	{
		$helper = $this->getHelper('question');

		$question = new ChoiceQuestion(
			'Please select your favorite colors (defaults to red and blue)',
			array('cities', 'something', 'test'),
			'0'
		);

		$answer = $helper->ask($input, $output, $question);

		while($answer != 'end')
		{
			$output->writeln($answer);

			$answer = $helper->ask($input, $output, $question);
		}
	}
}