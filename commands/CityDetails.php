<?php namespace Ikariam\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CityDetails extends Command
{
	/**
	 * {@inheritdoc}
	 */
	public function configure()
	{
		$this->setName('cities:each');
		$this->setDescription('Calls city {id} on each city.');
	}



	/**
	 * {@inheritdoc}
	 */
	protected function execute(InputInterface $input, OutputInterface $output)
	{
		exec('php ikariam login');

		ob_start();

		passthru('php ikariam cities');

		$cities = ob_get_clean();

		$ids = [];

		foreach (explode("\n", $cities) as $city)
		{
			$atts = explode('|', $city);

			$atts = array_map(function ($att)
			{
				return trim($att);
			}, $atts);

			if (isset($atts[1]) && is_numeric($atts[1]))
			{
				$ids[] = $atts[1];
			}
		}

		foreach ($ids as $id)
		{
			passthru('php ikariam city ' . $id);
		}
	}

}