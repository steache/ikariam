<?php namespace Ikariam\Command;

use Ikariam\Enum\Resource;
use Ikariam\Manager\City;
use Ikariam\Manager\Request;
use Ikariam\Util\TimeFormat;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class CityDetail
 * @package Ikariam\Command
 */
class CityDetail extends Command
{
	/**
	 * {@inheritdoc}
	 */
	protected function configure()
	{
		$this->setName('city');
		$this->addArgument('id', InputArgument::REQUIRED, 'Id of city');
		$this->setDescription('Lists detail of city');
	}



	/**
	 * {@inheritdoc}
	 */
	protected function execute(InputInterface $input, OutputInterface $output)
	{
		exec('php ikariam login');

		$atts = require_once(PATH . 'config.php');

		$atts['cachePath'] = CACHE_PATH;

		$requstManager = new Request($atts);

		$cityManager = new City($requstManager);

		$city = $cityManager->getCityById($input->getArgument('id'));

		$timeManager = new TimeFormat();

		$rows = [
			['ID', $city->getId()],
			['NAME', $city->getName()],
			['COORDS', implode(':', $city->getCoords())],
			['EXTRA', $city->getResourceText()],
			['POPULATION', $city->getPopulation()],
			['CITIZENS', $city->getCitizens()],
			['WINE_CONSUMPTION', $city->getWineSpending(), $city->getWineSpending()],
		];

		if($city->getExtraResource() == Resource::WINE)
		{
			$rows[6][3] = round($city->getTradegoodProduction(), 2);
		}

		foreach ($city->getResources() as $key => $resource)
		{
			$production = 0;
			$percent = round($resource['current'] / ($resource['max']/100), 2);
			$eta = '~';

			if($key == Resource::WOOD && $city->getResourceProduction() > 0)
			{
				$production = $city->getResourceProduction();
				$etaSeconds = ($resource['max'] - $resource['current']) / $production;
				$eta = $timeManager->getInUnits($etaSeconds);
			}

			if($key == $city->getExtraResource() && $city->getTradegoodProduction() > 0)
			{
				$production = $city->getTradegoodProduction();
				$etaSeconds = ($resource['max'] - $resource['current']) / $production;
				$eta = $timeManager->getInUnits($etaSeconds);
			}

			$rows[] = [$city->getResourceText($key), $resource['current'], $resource['max'], round($production*60*60, 2), $percent, $eta];
		}

		$table = new Table($output);

		$table->setHeaders(['KEY', 'CURRENT', 'MAX', 'PROD', '%', 'ETA']);

		$table->addRows($rows);

		$table->render();
	}

}