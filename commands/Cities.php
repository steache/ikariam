<?php namespace Ikariam\Command;

use Ikariam\Manager\City;
use Ikariam\Manager\Request;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class Cities
 * @package Ikariam\Command
 */
class Cities extends Command
{
	/**
	 * {@inheritdoc}
	 */
	public function configure()
	{
		$this->setName('cities');
		$this->setDescription('Lists all cities');
	}



	/**
	 * {@inheritdoc}
	 */
	protected function execute(InputInterface $input, OutputInterface $output)
	{
		exec('php ikariam login');

		$atts = require_once(PATH . 'config.php');

		$atts['cachePath'] = CACHE_PATH;

		$requstManager = new Request($atts);

		$cityManager = new City($requstManager);

		$cities = $cityManager->getCities();
		$rows   = [];

		foreach ($cities as $city)
		{
			$rows[] = [
				$city->getId(),
				$city->getName(),
				implode(':', $city->getCoords()),
				$city->getResourceText()
			];
		}

		$table = new Table($output);

		$table->setHeaders(['ID', 'NAME', 'COORDS', 'RESOURCE']);

		$table->addRows($rows);

		$table->render();
	}
}