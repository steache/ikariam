<?php namespace Ikariam\Command;

use Ikariam\IkariamException;
use Ikariam\Manager\Island;
use Ikariam\Manager\Request;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class Islandsbycoords
 * @package App\Command
 */
class Islandsbycoords extends Command
{
	/**
	 * {@inheritdoc}
	 */
	public function configure()
	{
		$this->setName('islands:coords')
			 ->addArgument('x', InputArgument::REQUIRED)
			 ->addArgument('y', InputArgument::REQUIRED)
			 ->setDescription('Find island by x & y');
	}



	/**
	 * {@inheritdoc}
	 */
	public function execute(InputInterface $input, OutputInterface $output)
	{
		exec('php ikariam login');

		$atts = require_once(PATH . 'config.php');

		$atts['cachePath'] = CACHE_PATH;

		$requstManager = new Request($atts);

		$islandManager = new Island($requstManager);

		try
		{
			$island = $islandManager->getByCoords($input->getArgument('x'), $input->getArgument('y'));
			$output->writeln('Found: ' . $island->getName() . ', on coords: [' . implode(':', $island->getCoords()) .
							 ']');
		}
		catch (IkariamException $e)
		{
			$output->writeln($e->getMessage());
		}
	}
}