<?php namespace Ikariam\Command;

use Ikariam\IkariamException;
use Ikariam\Manager\Request;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class Login
 * @package Ikariam\Command
 */
class Login extends Command
{
	/**
	 * {@inheritdoc}
	 */
	public function configure()
	{
		$this->setName('login');
		$this->setDescription('Tryies to login into the game.');
	}



	/**
	 * {@inheritdoc}
	 */
	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$atts = require_once(PATH . 'config.php');

		$atts['cachePath'] = CACHE_PATH;

		$requestManager = new Request($atts);

		try
		{
			$requestManager->login();

			$output->writeln('Logged in!');
		}
		catch (IkariamException $e)
		{
			$output->writeln($e->getCode() . ' : ' . $e->getMessage());
		}
	}
}