<?php namespace Ikariam\Command;

use Ikariam\Manager\Island;
use Ikariam\Manager\Request;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class Listislands
 * @package App\Command
 */
class Listislands extends Command
{
	/**
	 * Konfigurace
	 */
	public function configure()
	{
		$this->setName('islands')
			 ->addArgument('xmin', InputArgument::OPTIONAL, 'Minimal x')
			 ->addArgument('xmax', InputArgument::OPTIONAL, 'Maximal x')
			 ->addArgument('ymin', InputArgument::OPTIONAL, 'Minimal y')
			 ->addArgument('ymax', InputArgument::OPTIONAL, 'Maximal y')
			 ->setDescription('Lists islands in given area or all');
	}



	public function execute(InputInterface $input, OutputInterface $output)
	{
		exec('php ikariam login');

		$atts = require_once(PATH . 'config.php');

		$atts['cachePath'] = CACHE_PATH;

		$requstManager = new Request($atts);

		$islandManager = new Island($requstManager);

		$islands = ($input->getArgument('xmin') != null && $input->getArgument('ymax') != null)
			? $islandManager->getByArea(
				$input->getArgument('xmin'),
				$input->getArgument('xmax'),
				$input->getArgument('ymin'),
				$input->getArgument('ymax')
			)
			: $islandManager->getIslands();

		$rows = [];

		foreach ($islands as $island)
		{
			$rows[] = [
				$island->getId(),
				$island->getName(),
				implode(':', $island->getCoords()),
				$island->getExtraResource(),
				$island->getCityCount() . '/17',
			];
		}

		$table = new Table($output);

		$table->setHeaders(['ID', 'NAME', 'COORDS', 'RESOURCE', 'CITIES']);

		$table->setRows($rows);

		$table->render();
	}
}