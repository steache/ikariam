<?php namespace Ikariam\Command;

use Ikariam\Enum\Resource;
use Ikariam\IkariamException;
use Ikariam\Manager\City;
use Ikariam\Manager\Request;
use Ikariam\Manager\Transport;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;

/**
 * Class Sendtransport
 * @package App\Command
 */
class Sendtransport extends Command
{
	/**
	 * {@inheritdoc}
	 */
	public function configure()
	{
		$this->setName('transport:tutorial')
			 ->setDescription('Starts transporting tutorial :)');
	}



	/**
	 * {@inheritdoc}
	 */
	public function execute(InputInterface $input, OutputInterface $output)
	{
		$atts = require_once(PATH . 'config.php');

		$atts['cachePath'] = CACHE_PATH;

		$requestManager = new Request($atts);

		try
		{
			$requestManager->login();
		}
		catch(IkariamException $e)
		{
			$output->writeln('Nepodařilo se připojit.');
			die();
		}

		$cityManager = new City($requestManager);

		$cities = $cityManager->getCities();

		$helper = $this->getHelper('question');

		$names = [];

		foreach ($cities as $city)
		{
			$names[] = $city->getName();
		}

		$question     = new ChoiceQuestion('From which city? (def maincity)', $names, 0);
		$fromCityName = $helper->ask($input, $output, $question);

		$question   = new ChoiceQuestion('To which city? (def SIM II)', $names, 2);
		$toCityName = $helper->ask($input, $output, $question);

		$fromCity = null;
		$toCity   = null;

		foreach ($cities as $city)
		{
			if ($city->getName() == $fromCityName)
			{
				$fromCity = $city;
			}

			if ($city->getName() == $toCityName)
			{
				$toCity = $city;
			}
		}

		$resources = [
			Resource::WOOD          => 'WOOD',
			Resource::WINE          => 'WINE',
			Resource::MARBLE        => 'MARBLE',
			Resource::CRYSTAL_GLASS => 'CRYSTAL_GLASS',
			Resource::SULFUR        => 'SULFUR'
		];

		$question = new ChoiceQuestion('Which resource (def wood)', $resources, 0);
		$resource = $helper->ask($input, $output, $question);

		$question = new Question('How many resources? (def 500) -> just hit enter', 500);
		$amount   = $helper->ask($input, $output, $question);

		try
		{
			$fromCity = $cityManager->getCityById($fromCity->getId());
			$toCity = $cityManager->getCityById($toCity->getId());

			$transportManager = new Transport($requestManager);

			$transportManager->transport(
				$fromCity,
				$toCity,
				[
					array_search($resource, $resources) => $amount
				]);

			$output->writeln('OK sent ;)');
		}
		catch (IkariamException $e)
		{
			$output->writeln($e->getMessage());
		}
	}
}