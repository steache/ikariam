<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=1" />
        <title>
            Ikariam -
            Svět Ny	</title>
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
    <link rel="apple-touch-icon-precomposed" href="apple-touch-icon-precomposed.png" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />
    <link href="/skin/compiled-ltr-common_0.css?rev=51647"  rel="stylesheet" type="text/css" />
    <link href="/skin/compiled-ltr-common_1.css?rev=51647"  rel="stylesheet" type="text/css" />
    <link href="/skin/compiled-cz-city.css?rev=51647"  rel="stylesheet" type="text/css" />
    <link href="http://gf1.geo.gfsrv.net/cdn3d/5d8bf9f90d52dee6af8f108121b1fa.css"  rel="stylesheet"  type="text/css" />
     <link href="http://gf1.geo.gfsrv.net/cdn3f/295aef6579fe3bcaba0ac4e9d9be86.css" rel="stylesheet" type="text/css" />
     
</head>
<body id="city" class="flexible">


    <div id="container"  style="width: ; height: ; overflow-y: hidden;">
        <span class="accesshint">Ikariam</span>
        <span class="accesshint">Žijte v antickém světě!</span>
               <div id="GF_toolbar">
    <ul>
        <li class="ikhelp">
            <a onclick="ajaxHandlerCall(this.href);return false;" class="noViewParameters" href="?view=ikipedia&helpId=0"
               title="Pomoc"> Pomoc</a>
        </li>
                <li class="premium">
            <a id="js_premiumLinkInHeadline" class="noViewParameters" onclick="ajaxHandlerCall(this.href);return false;"
               href="?view=premium" title="Ikariam PLUS">
                <span class="textLabel">Ikariam PLUS (<span
                    id="headlineAmbrosia">29</span>)</span>
            </a>
        </li>
                <li class="highscore">
            <a onclick="ajaxHandlerCall(this.href);return false;" class="noViewParameters"
               href="/index.php?view=highscore&showMe=1" title="Hodnocení">
                Hodnocení            </a>
        </li>
        <li class="options">
            <a onclick="ajaxHandlerCall(this.href);return false;" class="noViewParameters"
               href="/index.php?view=options" title="Možnosti">
                Možnosti            </a>
        </li>
        <li class="notes">
            <a href="javascript:ikariam.show('avatarNotes')" title="Poznámky">
                Poznámky            </a>
        </li>
        <li class="forum">
            <a href="http://board.cz.ikariam.gameforge.com" title="Fórum" target="_blank">
                Fórum            </a>
        </li>
                <li class="">
            <a onclick="ajaxHandlerCall(this.href);return false;" class="noViewParameters"
               href="?view=avatarDetails" title="Podrobnosti o hráči">
                Podrobnosti o hráči            </a>
        </li>
                <li class="logout">
            <a href="/index.php?action=logoutAvatar&function=logout"
               title="Ukončit herní relaci">
                Odhlásit            </a>
        </li>
                <li class="version">
            <a onclick="ajaxHandlerCall(this.href);return false;" class="noViewParameters" href="?view=version"
               title="Version">
                <span
                    title="Verze">v 0.6.6</span></a>
        </li>
                <li class="serverTime"><a><span id="servertime">25.05.2015 16:47:54</span></a></li>
            </ul>
</div>
        <div id="header">
                    <div id="topnavi">

                        <div id="topnaviResources">
                 

<div id="globalResources">
    <ul>
        <li class="ambrosiaNoSpin" title="29 Ambrózie">
            <a id="js_GlobalMenu_ambrosia" class="noViewParameters" onclick="ajaxHandlerCall(this.href);return false;" href="?view=premium">Shop </a>
        </li>	
        <li class="transporters" title="Obchodní lodě K dispozici/Celkem">
            <a class="noViewParameters" onclick="ajaxHandlerCall(this.href);return false;"  href="?view=merchantNavy">
                <span id="js_GlobalMenu_freeTransporters">41</span> (<span id="js_GlobalMenu_maxTransporters">41</span>)
            </a>
        </li>                            
        <li class="gold" title="246,781 Zlato">
            <a id="js_GlobalMenu_gold" class="noViewParameters" onclick="ajaxHandlerCall(this.href);return false;" href="?view=finances">246,781</a>
        </li>
    </ul>
</div>

<div id="cityResources">    
    <ul class="resources">
          <li id="resources_population" class="population " title="Populace">
            <span id="js_GlobalMenu_citizens">1,183</span> (<span id="js_GlobalMenu_population">1,462</span>)
        </li>

        <li id="js_GlobalMenu_maxActionPoints" class="actions" title="Akční body">7</li>
        <li id="resources_foreign" class=" invisible bold red"></li>
          <li id="resources_wood"
            class="wood "
                    onclick="ajaxHandlerCall('?view=island&dialog=resource');return false;" style="cursor:pointer;"
                >
        




        <span id="js_GlobalMenu_wood"
              class="">59,860</span>

        <div id="js_GlobalMenu_wood_tooltip"
             class="tooltip"
             style="line-height:13px;">

            <p class="smallFont">
                Ve skladišti:
                    <span id="js_GlobalMenu_wood_Total">
                        59,860                    </span>
            </p>
                            <p class="smallFont">Hodinová produkce Stavební materiál:
                    <span
                        id="js_GlobalMenu_resourceProduction">200</span>
                </p>
            
            <p class="smallFont">Skladovací kapacita Stavební materiál: <span
                    id="js_GlobalMenu_max_wood">226,500</span>
            </p>

            <p class="smallFont">Tržiště: <span
                    id="js_GlobalMenu_branchOffice_wood">0</span>
            </p>

                    </div>
    </li>
        <li id="resources_wine"
            class="wine "
                >
        




        <span id="js_GlobalMenu_wine"
              class="">28,095</span>

        <div id="js_GlobalMenu_wine_tooltip"
             class="tooltip"
             style="line-height:13px;">

            <p class="smallFont">
                Ve skladišti:
                    <span id="js_GlobalMenu_wine_Total">
                        28,095                    </span>
            </p>
                            <p class="smallFont">
                    Výdaje na hodinu:
                    <span id="js_GlobalMenu_WineConsumption">
                        110                    </span>
                </p>
                            <p id="js_GlobalMenu_production_container_wine"
                   class="smallFont invisible">Hodinová produkce Víno                    :
                    <span
                        id="js_GlobalMenu_production_wine">0</span>
                </p>
            
            <p class="smallFont">Skladovací kapacita Víno: <span
                    id="js_GlobalMenu_max_wine">226,500</span>
            </p>

            <p class="smallFont">Tržiště: <span
                    id="js_GlobalMenu_branchOffice_wine">0</span>
            </p>

                    </div>
    </li>
        <li id="resources_marble"
            class="marble "
                >
        




        <span id="js_GlobalMenu_marble"
              class="">8,358</span>

        <div id="js_GlobalMenu_marble_tooltip"
             class="tooltip"
             style="line-height:13px;">

            <p class="smallFont">
                Ve skladišti:
                    <span id="js_GlobalMenu_marble_Total">
                        8,358                    </span>
            </p>
                            <p id="js_GlobalMenu_production_container_marble"
                   class="smallFont invisible">Hodinová produkce Mramor                    :
                    <span
                        id="js_GlobalMenu_production_marble">0</span>
                </p>
            
            <p class="smallFont">Skladovací kapacita Mramor: <span
                    id="js_GlobalMenu_max_marble">226,500</span>
            </p>

            <p class="smallFont">Tržiště: <span
                    id="js_GlobalMenu_branchOffice_marble">0</span>
            </p>

                    </div>
    </li>
        <li id="resources_glass"
            class="glass "
                >
        




        <span id="js_GlobalMenu_crystal"
              class="">2,842</span>

        <div id="js_GlobalMenu_crystal_tooltip"
             class="tooltip"
             style="line-height:13px;">

            <p class="smallFont">
                Ve skladišti:
                    <span id="js_GlobalMenu_crystal_Total">
                        2,842                    </span>
            </p>
                            <p id="js_GlobalMenu_production_container_crystal"
                   class="smallFont invisible">Hodinová produkce Krystalické Sklo                    :
                    <span
                        id="js_GlobalMenu_production_crystal">0</span>
                </p>
            
            <p class="smallFont">Skladovací kapacita Krystalické Sklo: <span
                    id="js_GlobalMenu_max_crystal">226,500</span>
            </p>

            <p class="smallFont">Tržiště: <span
                    id="js_GlobalMenu_branchOffice_crystal">0</span>
            </p>

                    </div>
    </li>
        <li id="resources_sulfur"
            class="sulfur "
                    onclick="ajaxHandlerCall('?view=island&dialog=tradegood&type=4');return false;" style="cursor:pointer;"
                >
        




        <span id="js_GlobalMenu_sulfur"
              class="">21,517</span>

        <div id="js_GlobalMenu_sulfur_tooltip"
             class="tooltip"
             style="line-height:13px;">

            <p class="smallFont">
                Ve skladišti:
                    <span id="js_GlobalMenu_sulfur_Total">
                        21,517                    </span>
            </p>
                            <p id="js_GlobalMenu_production_container_sulfur"
                   class="smallFont ">Hodinová produkce Síra                    :
                    <span
                        id="js_GlobalMenu_production_sulfur">0</span>
                </p>
            
            <p class="smallFont">Skladovací kapacita Síra: <span
                    id="js_GlobalMenu_max_sulfur">226,500</span>
            </p>

            <p class="smallFont">Tržiště: <span
                    id="js_GlobalMenu_branchOffice_sulfur">0</span>
            </p>

                    </div>
    </li>
    </ul>
</div>
            </div>

                        <div id="cityNav">
                 <form id="changeCityForm" onsubmit="ajaxHandlerCallFromForm(this);return false;" method="post">
                    <input type="hidden" name="action" value="header" />
                    <input type="hidden" name="function" value="changeCurrentCity" />
                    <input type="hidden" name="isMission" value="1" /> 
                    <input type="hidden" id="js_ChangeCityActionRequest" name="actionRequest" value="f8f5f370134d46cb215c82f92fcee9a0" />
                    <input type="hidden" name="oldView" value="city" />
                    <input id="js_cityIdOnChange" type="hidden" name="cityId" value="0" />
                    <div id="js_citySelectContainer" class="select_container city_select shorten_text"> </div>

                    <div class="city_nav_button viewWorldmap" id=js_worldMapLink >
        <a class="smallFont" onclick="ajaxHandlerCall(this.href);return false;"  href="?view=worldmap_iso" tabindex="4" title="Centrovat zvolené město na Světové Mapě">
            Zobrazit svět        </a>
   </div>
    <div class="city_nav_button viewIsland" id=js_islandLink >
        <a class="smallFont" onclick="ajaxHandlerCall(this.href);return false;"  href="?view=island" tabindex="5" title="Přejít na ostrovní mapu zvoleného města">
            Zobrazit ostrov        </a>
   </div>
    <div class="city_nav_button viewCity" id=js_cityLink >
        <a class="smallFont" onclick="ajaxHandlerCall(this.href);return false;"  href="?view=city" tabindex="6" title="Inspekce zvoleného města">
            Zobrazit město        </a>
   </div>
                </form>
            </div>
        </div>

                <div id="advisors">
            <ul>
                <li id="advCities" class="advisor_bubble" title="ECONOMY">
                    <a id="js_GlobalMenu_cities" onclick="ajaxHandlerCall(this.href);return false;" href="?view=tradeAdvisor&oldView=city&cityId=100470" title="Přehled měst a financí" class="normalactive">
                        <span class="smallFont">Města</span>
                    </a>
                    <a id="js_GlobalMenu_citiesPremium" class="plus_button " onclick="ajaxHandlerCall(this.href);return false;" href="" title="Na Přehled"></a>
               </li>

                <li id="advMilitary" class="advisor_bubble" title="MILITARY">
                    <a id="js_GlobalMenu_military" onclick="ajaxHandlerCall(this.href);return false;" href="?view=militaryAdvisor&oldView=city&cityId=100470" title="Přehled armády" class="normal">
                        <span class="smallFont">Armáda</span>
                    </a>
                     <a id="js_GlobalMenu_militaryPremium" class="plus_button " onclick="ajaxHandlerCall(this.href);return false;" href="" title="Na Přehled"></a>
                </li>

                <li id="advResearch" class="advisor_bubble" title="SCIENCE">
                    <a id="js_GlobalMenu_research" onclick="ajaxHandlerCall(this.href);return false;" href="?view=researchAdvisor&oldView=city&cityId=100470" title="Přehled výzkumů" class="normal">
                        <span class="smallFont">Výzkum</span>
                    </a>
                    <a id="js_GlobalMenu_researchPremium" class="plus_button " onclick="ajaxHandlerCall(this.href);return false;" href="" title="Na Přehled"></a>
                </li>

                <li id="advDiplomacy" class="advisor_bubble" title="DIPLOMACY">
                    <a id="js_GlobalMenu_diplomacy" onclick="ajaxHandlerCall(this.href);return false;" href="?view=diplomacyAdvisor&oldView=city&cityId=100470" title="Přehled zpráv a diplomacie" class="normal">
                        <span class="smallFont">Diplomacie</span>
                    </a>
                    <a id="js_GlobalMenu_diplomacyPremium" class="plus_button " onclick="ajaxHandlerCall(this.href);return false;" href="" title="Na Přehled"></a>
                </li>
            </ul>
        </div>
        <div class="clearboth"></div>
    </div>

    <div id="avatarNotes" class="popup_window focusable" style="display: none;"></div>
    <div id="chatWindow" class="popup_window focusable" style="display: none;"></div>
    <div id="popupMessage" class="popup_window focusable"></div>
    <div id="breadcrumbs" class="breadcrumbs">
    <a id="js_worldBread" href="?view=worldmap_iso&islandX=54&islandY=60" class="yellow" title="Zpět na světovou mapu">
        <img src="skin/layout/icon-world.png" alt="Svět" class="vertical_middle" /> Svět    </a>
    <span class="white"> &gt; </span>
    <a id="js_islandBread" class="yellow" href="?view=island&islandId=244" title="Zpět na ostrov">
        <img src="skin/layout/icon-island.png" class="vertical_middle" />
        <span id="js_islandBreadName" class="yellow">Tejios</span>
        <span id="js_islandBreadCoords"> [54:60]</span>
    </a>
    <span class="white"> &gt; </span>
    <span class="white" id="js_cityBread">Simplicity I</span>
    
    <div id="js_cityWarningBox" class="occupation_warning red_box"> 
        <div id="js_cityOccupiedText" class="occupied_warning clearfix"></div> 
        <div id="js_harbourOccupiedText" class="occupied_warning clearfix"></div> 
        <div id="js_spiesInsideText" class="spy_warning clearfix"></div> 
    </div>  
</div>



<div id="worldview" onselectstart="return false" style="                                                                               ">
    <div id="scrollcover">

        <div id="worldmap" class="phase4" style="                                                                                                                                                      top: -160px;                                                                           left: -1290px;                                                                                                                                              -moz-transform: scale(1);
                                                                       -ms-transform: scale(1);
                                                                       -o-transform: scale(1);
                                                                       -webkit-transform: scale(1);
                                                                       transform: scale(1);">
            <div id="locations" style="z-index: 200;">
                             <div id="position0" class="position0 building townHall level17">
                   <div id="js_CityPosition0Img" class="buildingimg img_pos animated"></div>

                                        <div class="hover img_pos invisible"></div>
                     <a id="js_CityPosition0Link" class="hoverable" onclick="ajaxHandlerCall(this.href);return false;"  href="javascript:void(0);"></a>

                   
                   


                    
                  </div>
                    <div id="js_CityPosition0Countdown" style="cursor:pointer;" class="position0 cityScroll">
                        <div class="before"></div>
                        <div class="buildingUpgradeIcon" id="js_CityPosition0CountdownText">
                                                    </div>
                        <div id="js_CityPosition0SpeedupButton" title="Zkrátit čas výstavby"></div>
                        <div class="after"></div>
                    </div>

                             <div id="position1" class="position1 building port level10">
                   <div id="js_CityPosition1Img" class="buildingimg img_pos animated"></div>

                                        <div class="hover img_pos invisible"></div>
                     <a id="js_CityPosition1Link" class="hoverable" onclick="ajaxHandlerCall(this.href);return false;"  href="javascript:void(0);"></a>

                   
                   


                                             <div id="js_CityPosition1PortCountdown" style="cursor:pointer;">
                             <div class="before"></div>
                             <div class="portLoadingIcon" id="js_CityPosition1PortCountdownText"></div>
                             <div id="js_CityPosition1PortSpeedupButton"></div>
                             <div class="after"></div>
                         </div>
                    
                  </div>
                    <div id="js_CityPosition1Countdown" style="cursor:pointer;" class="position1 cityScroll">
                        <div class="before"></div>
                        <div class="buildingUpgradeIcon" id="js_CityPosition1CountdownText">
                                                    </div>
                        <div id="js_CityPosition1SpeedupButton" title="Zkrátit čas výstavby"></div>
                        <div class="after"></div>
                    </div>

                             <div id="position2" class="position2 building shipyard level6">
                   <div id="js_CityPosition2Img" class="buildingimg img_pos animated"></div>

                                        <div class="hover img_pos invisible"></div>
                     <a id="js_CityPosition2Link" class="hoverable" onclick="ajaxHandlerCall(this.href);return false;"  href="javascript:void(0);"></a>

                   
                   


                                             <div id="js_CityPosition2PortCountdown" style="cursor:pointer;">
                             <div class="before"></div>
                             <div class="portLoadingIcon" id="js_CityPosition2PortCountdownText"></div>
                             <div id="js_CityPosition2PortSpeedupButton"></div>
                             <div class="after"></div>
                         </div>
                    
                  </div>
                    <div id="js_CityPosition2Countdown" style="cursor:pointer;" class="position2 cityScroll">
                        <div class="before"></div>
                        <div class="buildingUpgradeIcon" id="js_CityPosition2CountdownText">
                                                    </div>
                        <div id="js_CityPosition2SpeedupButton" title="Zkrátit čas výstavby"></div>
                        <div class="after"></div>
                    </div>

                             <div id="position3" class="position3 building carpentering level15">
                   <div id="js_CityPosition3Img" class="buildingimg img_pos animated"></div>

                                        <div class="hover img_pos invisible"></div>
                     <a id="js_CityPosition3Link" class="hoverable" onclick="ajaxHandlerCall(this.href);return false;"  href="javascript:void(0);"></a>

                   
                   


                    
                  </div>
                    <div id="js_CityPosition3Countdown" style="cursor:pointer;" class="position3 cityScroll">
                        <div class="before"></div>
                        <div class="buildingUpgradeIcon" id="js_CityPosition3CountdownText">
                                                    </div>
                        <div id="js_CityPosition3SpeedupButton" title="Zkrátit čas výstavby"></div>
                        <div class="after"></div>
                    </div>

                             <div id="position4" class="position4 building safehouse level8">
                   <div id="js_CityPosition4Img" class="buildingimg img_pos animated"></div>

                                        <div class="hover img_pos invisible"></div>
                     <a id="js_CityPosition4Link" class="hoverable" onclick="ajaxHandlerCall(this.href);return false;"  href="javascript:void(0);"></a>

                   
                   


                    
                  </div>
                    <div id="js_CityPosition4Countdown" style="cursor:pointer;" class="position4 cityScroll">
                        <div class="before"></div>
                        <div class="buildingUpgradeIcon" id="js_CityPosition4CountdownText">
                                                    </div>
                        <div id="js_CityPosition4SpeedupButton" title="Zkrátit čas výstavby"></div>
                        <div class="after"></div>
                    </div>

                             <div id="position5" class="position5 building warehouse level14">
                   <div id="js_CityPosition5Img" class="buildingimg img_pos animated"></div>

                                        <div class="hover img_pos invisible"></div>
                     <a id="js_CityPosition5Link" class="hoverable" onclick="ajaxHandlerCall(this.href);return false;"  href="javascript:void(0);"></a>

                   
                   


                    
                  </div>
                    <div id="js_CityPosition5Countdown" style="cursor:pointer;" class="position5 cityScroll">
                        <div class="before"></div>
                        <div class="buildingUpgradeIcon" id="js_CityPosition5CountdownText">
                                                    </div>
                        <div id="js_CityPosition5SpeedupButton" title="Zkrátit čas výstavby"></div>
                        <div class="after"></div>
                    </div>

                             <div id="position6" class="position6 building museum level3">
                   <div id="js_CityPosition6Img" class="buildingimg img_pos animated"></div>

                                        <div class="hover img_pos invisible"></div>
                     <a id="js_CityPosition6Link" class="hoverable" onclick="ajaxHandlerCall(this.href);return false;"  href="javascript:void(0);"></a>

                   
                   


                    
                  </div>
                    <div id="js_CityPosition6Countdown" style="cursor:pointer;" class="position6 cityScroll">
                        <div class="before"></div>
                        <div class="buildingUpgradeIcon" id="js_CityPosition6CountdownText">
                                                    </div>
                        <div id="js_CityPosition6SpeedupButton" title="Zkrátit čas výstavby"></div>
                        <div class="after"></div>
                    </div>

                             <div id="position7" class="position7 building workshop level8">
                   <div id="js_CityPosition7Img" class="buildingimg img_pos animated"></div>

                                        <div class="hover img_pos invisible"></div>
                     <a id="js_CityPosition7Link" class="hoverable" onclick="ajaxHandlerCall(this.href);return false;"  href="javascript:void(0);"></a>

                   
                   


                    
                  </div>
                    <div id="js_CityPosition7Countdown" style="cursor:pointer;" class="position7 cityScroll">
                        <div class="before"></div>
                        <div class="buildingUpgradeIcon" id="js_CityPosition7CountdownText">
                                                    </div>
                        <div id="js_CityPosition7SpeedupButton" title="Zkrátit čas výstavby"></div>
                        <div class="after"></div>
                    </div>

                             <div id="position8" class="position8 building temple level3">
                   <div id="js_CityPosition8Img" class="buildingimg img_pos animated"></div>

                                        <div class="hover img_pos invisible"></div>
                     <a id="js_CityPosition8Link" class="hoverable" onclick="ajaxHandlerCall(this.href);return false;"  href="javascript:void(0);"></a>

                   
                   


                    
                  </div>
                    <div id="js_CityPosition8Countdown" style="cursor:pointer;" class="position8 cityScroll">
                        <div class="before"></div>
                        <div class="buildingUpgradeIcon" id="js_CityPosition8CountdownText">
                                                    </div>
                        <div id="js_CityPosition8SpeedupButton" title="Zkrátit čas výstavby"></div>
                        <div class="after"></div>
                    </div>

                             <div id="position9" class="position9 building barracks level12">
                   <div id="js_CityPosition9Img" class="buildingimg img_pos animated"></div>

                                        <div class="hover img_pos invisible"></div>
                     <a id="js_CityPosition9Link" class="hoverable" onclick="ajaxHandlerCall(this.href);return false;"  href="javascript:void(0);"></a>

                   
                   


                    
                  </div>
                    <div id="js_CityPosition9Countdown" style="cursor:pointer;" class="position9 cityScroll">
                        <div class="before"></div>
                        <div class="buildingUpgradeIcon" id="js_CityPosition9CountdownText">
                                                    </div>
                        <div id="js_CityPosition9SpeedupButton" title="Zkrátit čas výstavby"></div>
                        <div class="after"></div>
                    </div>

                             <div id="position10" class="position10 building palace level4">
                   <div id="js_CityPosition10Img" class="buildingimg img_pos animated"></div>

                                        <div class="hover img_pos invisible"></div>
                     <a id="js_CityPosition10Link" class="hoverable" onclick="ajaxHandlerCall(this.href);return false;"  href="javascript:void(0);"></a>

                   
                   


                    
                  </div>
                    <div id="js_CityPosition10Countdown" style="cursor:pointer;" class="position10 cityScroll">
                        <div class="before"></div>
                        <div class="buildingUpgradeIcon" id="js_CityPosition10CountdownText">
                                                    </div>
                        <div id="js_CityPosition10SpeedupButton" title="Zkrátit čas výstavby"></div>
                        <div class="after"></div>
                    </div>

                             <div id="position11" class="position11 building branchOffice level8">
                   <div id="js_CityPosition11Img" class="buildingimg img_pos animated"></div>

                                        <div class="hover img_pos invisible"></div>
                     <a id="js_CityPosition11Link" class="hoverable" onclick="ajaxHandlerCall(this.href);return false;"  href="javascript:void(0);"></a>

                   
                   


                    
                  </div>
                    <div id="js_CityPosition11Countdown" style="cursor:pointer;" class="position11 cityScroll">
                        <div class="before"></div>
                        <div class="buildingUpgradeIcon" id="js_CityPosition11CountdownText">
                                                    </div>
                        <div id="js_CityPosition11SpeedupButton" title="Zkrátit čas výstavby"></div>
                        <div class="after"></div>
                    </div>

                             <div id="position12" class="position12 building academy level11">
                   <div id="js_CityPosition12Img" class="buildingimg img_pos animated"></div>

                                        <div class="hover img_pos invisible"></div>
                     <a id="js_CityPosition12Link" class="hoverable" onclick="ajaxHandlerCall(this.href);return false;"  href="javascript:void(0);"></a>

                   
                   


                    
                  </div>
                    <div id="js_CityPosition12Countdown" style="cursor:pointer;" class="position12 cityScroll">
                        <div class="before"></div>
                        <div class="buildingUpgradeIcon" id="js_CityPosition12CountdownText">
                                                    </div>
                        <div id="js_CityPosition12SpeedupButton" title="Zkrátit čas výstavby"></div>
                        <div class="after"></div>
                    </div>

                             <div id="position13" class="position13 building buildingGround land">
                   <div id="js_CityPosition13Img" class="buildingimg img_pos animated"></div>

                                        <div class="hover img_pos invisible"></div>
                     <a id="js_CityPosition13Link" class="hoverable" onclick="ajaxHandlerCall(this.href);return false;"  href="javascript:void(0);"></a>

                   
                   


                    
                  </div>
                    <div id="js_CityPosition13Countdown" style="cursor:pointer;" class="position13 cityScroll">
                        <div class="before"></div>
                        <div class="buildingUpgradeIcon" id="js_CityPosition13CountdownText">
                                                    </div>
                        <div id="js_CityPosition13SpeedupButton" title="Zkrátit čas výstavby"></div>
                        <div class="after"></div>
                    </div>

                             <div id="position14" class="position14 building wall level9">
                   <div id="js_CityPosition14Img" class="buildingimg img_pos animated"></div>

                                        <div class="hover img_pos invisible"></div>
                     <a id="js_CityPosition14Link" class="hoverable" onclick="ajaxHandlerCall(this.href);return false;"  href="javascript:void(0);"></a>

                   
                   


                    
                  </div>
                    <div id="js_CityPosition14Countdown" style="cursor:pointer;" class="position14 cityScroll">
                        <div class="before"></div>
                        <div class="buildingUpgradeIcon" id="js_CityPosition14CountdownText">
                                                    </div>
                        <div id="js_CityPosition14SpeedupButton" title="Zkrátit čas výstavby"></div>
                        <div class="after"></div>
                    </div>

                             <div id="position15" class="position15 building buildingGround land">
                   <div id="js_CityPosition15Img" class="buildingimg img_pos animated"></div>

                                        <div class="hover img_pos invisible"></div>
                     <a id="js_CityPosition15Link" class="hoverable" onclick="ajaxHandlerCall(this.href);return false;"  href="javascript:void(0);"></a>

                   
                   


                    
                  </div>
                    <div id="js_CityPosition15Countdown" style="cursor:pointer;" class="position15 cityScroll">
                        <div class="before"></div>
                        <div class="buildingUpgradeIcon" id="js_CityPosition15CountdownText">
                                                    </div>
                        <div id="js_CityPosition15SpeedupButton" title="Zkrátit čas výstavby"></div>
                        <div class="after"></div>
                    </div>

                             <div id="position16" class="position16 building warehouse level14">
                   <div id="js_CityPosition16Img" class="buildingimg img_pos animated"></div>

                                        <div class="hover img_pos invisible"></div>
                     <a id="js_CityPosition16Link" class="hoverable" onclick="ajaxHandlerCall(this.href);return false;"  href="javascript:void(0);"></a>

                   
                   


                    
                  </div>
                    <div id="js_CityPosition16Countdown" style="cursor:pointer;" class="position16 cityScroll">
                        <div class="before"></div>
                        <div class="buildingUpgradeIcon" id="js_CityPosition16CountdownText">
                                                    </div>
                        <div id="js_CityPosition16SpeedupButton" title="Zkrátit čas výstavby"></div>
                        <div class="after"></div>
                    </div>

                             <div id="position17" class="position17 building buildingGround sea">
                   <div id="js_CityPosition17Img" class="buildingimg img_pos animated"></div>

                                        <div class="hover img_pos invisible"></div>
                     <a id="js_CityPosition17Link" class="hoverable" onclick="ajaxHandlerCall(this.href);return false;"  href="javascript:void(0);"></a>

                   
                   


                    
                  </div>
                    <div id="js_CityPosition17Countdown" style="cursor:pointer;" class="position17 cityScroll">
                        <div class="before"></div>
                        <div class="buildingUpgradeIcon" id="js_CityPosition17CountdownText">
                                                    </div>
                        <div id="js_CityPosition17SpeedupButton" title="Zkrátit čas výstavby"></div>
                        <div class="after"></div>
                    </div>

                             <div id="position18" class="position18 building tavern level15">
                   <div id="js_CityPosition18Img" class="buildingimg img_pos animated"></div>

                                        <div class="hover img_pos invisible"></div>
                     <a id="js_CityPosition18Link" class="hoverable" onclick="ajaxHandlerCall(this.href);return false;"  href="javascript:void(0);"></a>

                   
                   


                    
                  </div>
                    <div id="js_CityPosition18Countdown" style="cursor:pointer;" class="position18 cityScroll">
                        <div class="before"></div>
                        <div class="buildingUpgradeIcon" id="js_CityPosition18CountdownText">
                                                    </div>
                        <div id="js_CityPosition18SpeedupButton" title="Zkrátit čas výstavby"></div>
                        <div class="after"></div>
                    </div>

                            <div id="city_background_nw" class="city_background"></div>
                <div id="city_background_ne" class="city_background"></div>
                <div id="city_background_sw" class="city_background"></div>
                <div id="city_background_se" class="city_background"></div>
                <div id="js_city_feature_garrisonGate" class="city_feature invisible" title="O toto město probíhá boj">
                </div>
                <div id="js_cityFight1" title="O toto město probíhá boj"></div>
                <div id="js_cityFight2" title="O toto město probíhá boj"></div>
                <div id="js_cityFight3" title="O toto město probíhá boj"></div>
                <div id="js_city_feature_garrison"
                     class="city_feature invisible"
                     title="Vaše město je okupované."
                     onclick="ajaxHandlerCall('?view=cityMilitary&activeTab=tabUnits');return false;"></div>


                <div id="js_city_feature_protester" class="city_feature invisible" title="Obyvatelé města jsou rozezlení!"></div>
                <div id="js_city_feature_beachboys" class="city_feature beachboys invisible">
                    <div class="animation animated_happy_citizen_1"></div>
                    <div class="animation animated_happy_citizen_2"></div>
                    <div class="animation animated_happy_citizen_3"></div>
                </div>

                                <div id="dolphinPos1" class="animation dolphin"></div>
                <div id="dolphinPos2" class="animation dolphin"></div>
                <div id="dolphinPos3" class="animation dolphin"></div>
                <div id="dolphinPos4" class="animation dolphin"></div>

                                <div class="animation animatedWell"></div>
                <div class="animation animatedRiverPart1"></div>
                <div class="animation animatedRiverPart2"></div>
                <div class="animation animatedRiverPart3"></div>
                <div class="animation animatedSea1"></div>
                <div class="animation animatedSea2"></div>
                                <div id="portOccupierShip1" class="invisible" onclick="ajaxHandlerCall('?view=cityMilitary&activeTab=tabShips');return false;"></div>
                <div id="portOccupierShip2" class="invisible" onclick="ajaxHandlerCall('?view=cityMilitary&activeTab=tabShips');return false;"></div>

                <div id="pirateFortressBackground"></div>
                <div id="pirateFortressShip"></div>
                <div id="walkers"></div>
                <a id="cityRegistrationGifts" class="invisible" onclick="ajaxHandlerCall('?view=registrationGifts');return false;"></a>


            </div>
            <div class="city_water_bottom" style="z-index: 100;"></div>
        </div>
    </div>
</div>








<div id="eventDiv">
    </div>


<script type="text/javascript" src="http://pixelzirkus.gameforge.com/functions.js"></script>
<script type="text/javascript">
setPixel ({
'location':'LOGIN',
'product':'ikariam',
'language':'cz',
'server-id':'13',
'user-id':'50260',
'user-email':'steache@gmail.com',
'user-name':'pickwick',
'last_login':'2015-05-25T16:47:13+0200'
});
</script>        <div id="js_viewChat" class="slot_menu chat_menu">
        <a href="#" onclick="javascript:ikariam.show('chatWindow');" alt="Alianční chat" class="">
            <h3 class="accesshint">Alianční chat</h3>
        </a>
    </div>
            <div id="leftMenu">
            <div id="js_viewCityMenu" class="slot_menu city_menu">
                <ul class="menu_slots">
                    <li class="expandable slot0 military"
                                                onclick="ajaxHandlerCall('?view=cityMilitary&activeTab=tabUnits&cityId=100470'); return false;">
                        <div class="image image_troops"></div>
                        <div class="name"><span class="namebox">Vojáci ve městě</span></div>
                    </li>
                    <li class="expandable slot0 espionage"
                        style="display: none;"                        onclick="ajaxHandlerCall('?view=spyMissions&targetCityId=100470'); return false;">
                        <div class="image image_espionage"></div>
                        <div class="name"><span class="namebox">Špionáž</span></div>
                    </li>
                    <li class="expandable resourceShop"
                                                onclick="ajaxHandlerCall('?view=premiumResourceShop&cityId=100470'); return false;">
                        <div class="image image_resourceShop"></div>
                        <div class="name"><span class="namebox">Získej suroviny!</span></div>
                    </li>
                    <li class="expandable slot1"
                                                onclick="ajaxHandlerCall('?view=premiumTrader'); return false;">
                        <div class="image image_trader"></div>
                        <div class="name"><span class="namebox">Prémiový Obchodník</span></div>
                    </li>
                    <li class="expandable slot2"
                                                onclick="ajaxHandlerCall('?view=friendListEdit'); return false;">
                        <div class="image image_friends"></div>
                        <div class="name"><span class="namebox">Pozvat přátele</span>
                        </div>
                    </li>
                    <li class="expandable slot3"
                        style="display: none;"                        onclick="ajaxHandlerCall('?action=header&function=setNavigation&screen=constructionList&cityId=100470&state=' + Math.abs(ikariam.getScreen().constructionListState-1)); return false;">
                        <div class="image image_constructionlist"></div>
                        <div class="name"><span class="namebox">Stavební řada</span></div>
                    </li>
                    <li class="expandable slot4"
                                                onclick="ajaxHandlerCall('?view=garrisonEdit&cityId=100470'); return false;">
                        <div class="image image_fireunit"></div>
                        <div class="name"><span class="namebox">Propustit</span></div>
                    </li>
                </ul>
            </div>
        </div>
        
    <div id="rightMenu">
        <div id="js_viewFriends" class="slot_menu friends_menu">
            <ul class="friends">
                                        <li id="js_friendlistSlot1" class="expandable">
                            <div class=" image"></div>
                            <div class="slotnum">1</div>
                            <div class="name">
                                <a href="?view=island&playerId=45390">budovatelpepa</a>
                                                            </div>
                                                            <div class="clan">
                                    <a onclick="ajaxHandlerCall(this.href);return false;"
                                       href="?view=allyPage&allyId=1666">-DVG-</a>
                                </div>
                                                                    <div title="je offline"
                                         class="activity idle"><span
                                            class="accesshint">je offline</span>
                                    </div>
                                                                <a onclick="ajaxHandlerCall(this.href);return false;"
                               title="Napsat zprávu"
                               href="?view=sendIKMessage&receiverId=45390&closeView=1"
                               class="sendmsg"><span
                                    class="accesshint">Napsat zprávu</span></a>
                            <a onclick="ajaxHandlerCall(this.href);return false;"
                               title="Ukončit přátelství"
                               href="?view=sendIKMessage&msgType=104&receiverId=45390&closeView=1"
                               class="delmsg"><span
                                    class="accesshint">Ukončit přátelství</span></a>
                        </li>
                                                <li id="js_friendlistSlot2" class="slot">
                            <div class="slotnum">2</div>
                            <a onclick="ajaxHandlerCall(this.href);return false;"
                               title="Pozvat přítele" href="?view=friendListEdit"
                               class="image"></a>
                        </li>
                                                <li id="js_friendlistSlot3" class="slot">
                            <div class="slotnum">3</div>
                            <a onclick="ajaxHandlerCall(this.href);return false;"
                               title="Pozvat přítele" href="?view=friendListEdit"
                               class="image"></a>
                        </li>
                                                <li id="js_friendlistSlot4" class="slot">
                            <div class="slotnum">4</div>
                            <a onclick="ajaxHandlerCall(this.href);return false;"
                               title="Pozvat přítele" href="?view=friendListEdit"
                               class="image"></a>
                        </li>
                                                <li id="js_friendlistSlot5" class="slot">
                            <div class="slotnum">5</div>
                            <a onclick="ajaxHandlerCall(this.href);return false;"
                               title="Pozvat přítele" href="?view=friendListEdit"
                               class="image"></a>
                        </li>
                                                <li id="js_friendlistSlot6" class="slot">
                            <div class="slotnum">6</div>
                            <a onclick="ajaxHandlerCall(this.href);return false;"
                               title="Pozvat přítele" href="?view=friendListEdit"
                               class="image"></a>
                        </li>
                                                <li id="js_friendlistSlot7" class="slot">
                            <div class="slotnum">7</div>
                            <a onclick="ajaxHandlerCall(this.href);return false;"
                               title="Pozvat přítele" href="?view=friendListEdit"
                               class="image"></a>
                        </li>
                                                <li id="js_friendlistSlot8" class="slot">
                            <div class="slotnum">8</div>
                            <a onclick="ajaxHandlerCall(this.href);return false;"
                               title="Pozvat přítele" href="?view=friendListEdit"
                               class="image"></a>
                        </li>
                                                <li id="js_friendlistSlot9" class="slot">
                            <div class="slotnum">9</div>
                            <a onclick="ajaxHandlerCall(this.href);return false;"
                               title="Pozvat přítele" href="?view=friendListEdit"
                               class="image"></a>
                        </li>
                                                <li id="js_friendlistSlot10" class="slot">
                            <div class="slotnum">10</div>
                            <a onclick="ajaxHandlerCall(this.href);return false;"
                               title="Pozvat přítele" href="?view=friendListEdit"
                               class="image"></a>
                        </li>
                                                <li id="js_friendlistSlot11" class="slot">
                            <div class="slotnum">11</div>
                            <a onclick="ajaxHandlerCall(this.href);return false;"
                               title="Pozvat přítele" href="?view=friendListEdit"
                               class="image"></a>
                        </li>
                                                <li id="js_friendlistSlot12" class="slot">
                            <div class="slotnum">12</div>
                            <a onclick="ajaxHandlerCall(this.href);return false;"
                               title="Pozvat přítele" href="?view=friendListEdit"
                               class="image"></a>
                        </li>
                                    </ul>
            <div id="toggleShowFriends" class="showfriends"></div>
            <a title="Další přátelé" href="#up" class="pageup"
                data-title="Další přátelé"><span
                    class="accesshint">Další přátelé</span></a>
            <a title="Další přátelé" href="#down" class="pagedown"><span
                    class="accesshint">Další přátelé</span></a>
        </div>
    </div>

      
        <div id="footer" class="clearfix">      
        <div class="footerbg">            
            <div id="mapControls" style="top:-7px">
        <ul class="scrolling">
            <li class="nw"><a href="#" onmousedown="mapIsClicked=true;ikariam.getMapNavigation().moveMap(-1,-1);" onmouseout="mapIsClicked=false;" onmouseup="mapIsClicked=false;"></a></li>
            <li class="n"><a href="#" onmousedown="mapIsClicked=true;ikariam.getMapNavigation().moveMap(0,-1);" onmouseout="mapIsClicked=false;" onmouseup="mapIsClicked=false;"></a></li>
            <li class="ne"><a href="#" onmousedown="mapIsClicked=true;ikariam.getMapNavigation().moveMap(1,-1); return false;" onmouseout="mapIsClicked=false;" onmouseup="mapIsClicked=false;"></a></li>
            <li class="w"><a href="#" onmousedown="mapIsClicked=true;ikariam.getMapNavigation().moveMap(-1,0); return false;" onmouseout="mapIsClicked=false;" onmouseup="mapIsClicked=false;"></a></li>
            <li class="e"><a href="#" onmousedown="mapIsClicked=true;ikariam.getMapNavigation().moveMap(1,0); return false;" onmouseout="mapIsClicked=false;" onmouseup="mapIsClicked=false;"></a> </li>
            <li class="sw"><a href="#" onmousedown="mapIsClicked=true;ikariam.getMapNavigation().moveMap(-1,1); return false;" onmouseout="mapIsClicked=false;" onmouseup="mapIsClicked=false;"></a></li>
            <li class="s"><a href="#" onmousedown="mapIsClicked=true;ikariam.getMapNavigation().moveMap(0,1); return false;" onmouseout="mapIsClicked=false;"  onmouseup="mapIsClicked=false;"></a></li>
            <li class="se"><a href="#" onmousedown="mapIsClicked=true;ikariam.getMapNavigation().moveMap(1,1); return false;" onmouseout="mapIsClicked=false;"   onmouseup="mapIsClicked=false;"></a></li>
            <li class="toggle"><a href="javascript:void(0);" onclick="return false;" onmouseup="ikariam.getMapNavigation().toggleControls(); return false;"></a></li>
        </ul>
 </div>

            
            <div id="js_toggleControlsOn" class="hideControls"></div>
            <div class="footerleft"> <span class="copyright">© 2008 by <a id="gflink" target="_blank" href="http://cz.gameforge.com">Gameforge 4D GmbH</a><a onclick="ajaxHandlerCall(this.href);return false;" href="/index.php?view=credits" style="margin:0px;">.</a> All rights reserved.</span> </div> 
                        <div class="footerright">      
                <ul class="footerlinks">
                    <li><a target="_blank" href="http://cz.ikariam.gameforge.com/rules.php" title="Pravidla">Pravidla</a></li>
                    <li><a href="http://agbserver.gameforge.com/csCZ-Terms-Ikariam.html" target="_blank" title="VOP">VOP</a></li>
                    <li><a href="http://agbserver.gameforge.com/csCZ-Imprint-Ikariam.html" target="_blank" title="Tiráž">Tiráž</a></li>
                    <li><a href="http://agbserver.gameforge.com/csCZ-Privacy-Ikariam.html" target="_blank" title="Ochrana osobních dat">Ochrana osobních dat</a></li>
                </ul>
            </div>
        </div>
    </div>

        
    

 
            <div id="loadingPreview"></div>
                    </div> 
                    <script type="text/javascript">
                console = {
                log: function() {},
                dir: function() {}
                };
            </script>
            
                    <script type="text/javascript" src="http://gf1.geo.gfsrv.net/cdn63/818f8e2c421aa2fc1ad140672e1827.js"></script>
            <script type="text/javascript" src="http://gf1.geo.gfsrv.net/cdn06/bd22abc7c3dd1d0cd6d098d1f51e75.js"></script>
            <script type="text/javascript" src="http://gf2.geo.gfsrv.net/cdnab/c103e0369261d751a48c10b82dfaca.js"></script>
        <script type="text/javascript">
    /**
     * CREATE GLOBAL DATASET FROM PHP (used on all screens)
     */
    w       = window;
    d       = w.document;
    jQuery.uuid = 1;

    /* some localization constants, used in various contexts... */
        LocalizationStrings = JSON.parse('{"ambrosia":"Ambr\u00f3zie","gold":"Zlato","tradegood":"M\u011bnit zbo\u017e\u00ed","wood":"Stavebn\u00ed materi\u00e1l","wine":"V\u00edno","marble":"Mramor","crystal":"Krystalick\u00e9 Sklo","sulfur":"S\u00edra","wood_deposit":"Les","wine_deposit":"Vinice","marble_deposit":"Mramorov\u00fd D\u016fl","crystal_deposit":"Krystalov\u00fd D\u016fl","sulfur_deposit":"S\u00edrov\u00e1 j\u00e1ma","wonder_name":"Z\u00e1zrak","send_message":"Odeslat Zpr\u00e1vu","citizens":"Obyvatel\u00e9","branchOffice":"Tr\u017ei\u0161t\u011b","upkeep":"\u00dadr\u017eba na hodinu. Lze sn\u00ed\u017eit prost\u0159ednictv\u00edm forem vl\u00e1dy a v\u00fdzkumem.","building_time":"\u010cas stavby","colonize_here":"Chcete zde zalo\u017eit kolonii?","avatar_banned":"Tento \u00fa\u010det je v t\u00e9to chv\u00edli zablokov\u00e1n!","avatar_vacation":"Hr\u00e1\u010d je moment\u00e1ln\u011b na dovolen\u00e9","avatar_inactive":"Hr\u00e1\u010d je v tuto chv\u00edli neaktivn\u00ed","avatar_inactive_suffix":"(n)","level":"\u00darove\u0148","free_building_space":"Voln\u00e1 Stavebn\u00ed Plocha","building_under_construction":"Ve v\u00fdstavb\u011b","yes":"Ano","abort":"Zru\u0161it","timeunits":{"short":{"year":"R","month":"M","day":"D","hour":"h","minute":"m","second":"s"}},"language":"cz","decimalPoint":".","thousandSeperator":",","warnings":{"tolargeText":"Pozor! V\u00e1\u0161 text je p\u0159\u00edli\u0161 dlouh\u00fd!"},"not_enought_resources":"St\u00e1le ti chyb\u00ed n\u011bkter\u00e9 suroviny!","friends":{"edit_title":"Pozvat p\u0159\u00edtele","send_msg":"Napsat zpr\u00e1vu","kick_msg":"Ukon\u010dit p\u0159\u00e1telstv\u00ed","online":"je online!","offline":"je offline","inactive":"nen\u00ed aktivn\u00ed"}}');

    /* is player in ally? */
    hasAlly = true;

    /* maximum chars for avatar notes */
    avatarNotesMaxChars = 200;

    /* the view on reload: city, island or worldmap. will get body-id */
    backgroundView = 'city';

    /* Data for Popup if needed */
    popupData = JSON.parse('null');

    /* initialize tutorial if required */
    tutorialData = null;

    
    var feedbackMsg = null;
    
    /* the shortcuts to islands with own cities and related cities for map navigation */
    islandShortcuts = new Array();
        var happyHourConfig = null;
        var ingameCounterCnt = 0;
    var ingameCounterConfig = {};
    var ingameCounterLinkedConfig = {};
                ingameCounterCnt = ingameCounterCnt + 1;
            ingameCounterConfig['25925'+ingameCounterCnt] = {
                timeLeft: 25925,
                popupTitle: 'Využijte tuto jedinečnou nabídku!',
                popupContent: ['<a href="?view=premiumPayment&oldView=premium" target=""><img height="106" src="http://gf1.geo.gfsrv.net/cdn9a/ed32692ed753d61410fd044748a510.jpg" style="float:none" /></a>Sluneční hodiny na Vašem tržišti to potvrzují, musíte si pospíšit! Šťastné hodiny začaly a rychle zase skončí. Zajistěte si rychle bonus 20 procent při nákupu ambrózií - teď během šťastných hodin na Ikariamu!'],
                popupButton: ['?view=premiumPayment&oldView=premium', 'Získat Ambrózie'],
                popupCloseButtonText: 'Zavřít',
                timeout: 1432591199,
                currentTime: '1432565274',
                scrollText: 'Šťastné hodiny<br />ještě:',
                scrollCssClass: '',
                scrollType: 'happyHour',
                counterType: 'popup'
            };
                       /**
            * CREATE DATASET FOR CITY
            */

            /* preparation for including city events (winter, halloween, easter...) - not implemented in v0-5-0 yet. */
                        localSpecialEvent = '';

            /* the building construction list. contains 0 or 1 building for non-premium users, premium users can have more buildings in queue. */
            constructionListEndDate = null, constructionListStartDate = null;

            
            var underConstruction = -1;
            var endUpgradeTime = -1;
            var startUpgradeTime = -1;
            
           var bgViewData = { currentCityId: 100470, underConstruction: underConstruction, endUpgradeTime: endUpgradeTime, startUpgradeTime: startUpgradeTime,
                              constructionListStart: constructionListStartDate, constructionListEnd: constructionListEndDate,
                              constructionListState: 0,
                              cityLeftMenu: {"visibility":{"military":1,"espionage":0,"resourceShop":1,"slot1":1,"slot2":1,"slot3":0,"slot4":1},"ownCity":1} };


    dataSetForView = {  /* the game's name */
                        gameName: "Ikariam",
                        /* the name of the server */
                        serverName: 'Ny',
                        /* the avatar id */
                        avatarId: '50260',
                        /* the avatar's ally id */
                        avatarAllyId: '1666',
                        /* the server time on reload */
                        serverTime: "1432565274",
                        realHour: 3600,
                        dateTimeFormat: "d.m.Y G:i:s",
                        serverTimezoneOffset: 7200,
                        /* the background view id (city, island or worldmap) */
                        backgroundView: backgroundView,
                         /* is the current city the player's own city? */
                        isOwnCity: true,
                        /* the luxury tradegood produced on current island */
                        producedTradegood: "4",
                        /* the player's current resource values in city */
                        currentResources: JSON.parse('{\"citizens\":1183.2432755683,\"population\":1462.2432755683,\"resource\":59860,\"2\":8358,\"1\":28095,\"4\":21517,\"3\":2842}'),
                        /* the maximum of resources that can be stored in city */
                        maxResources: JSON.parse('{\"resource\":226500,\"1\":226500,\"2\":226500,\"3\":226500,\"4\":226500}'),
                        /* the resources offered for sell in branch office */
                        branchOfficeResources: JSON.parse('{\"resource\":0,\"1\":0,\"2\":0,\"3\":0,\"4\":0}'),
                        /* the wood production amount in current city */
                        resourceProduction: 0.055555555555556,
                        /* the luxury tradegood production amount in current city  */
                        tradegoodProduction: 0,
                        /* wine spendings in city per hour */
                        wineSpendings: 110,
                        wineTickInterval: 3600,
                        /* the language direction ("rtl" or "ltr") */
                        languageDirection: "ltr",
                        /* does the player have an ally? */
                        hasAlly: hasAlly,
                        /* the saved state of the map navigation control elements */
                        mapNavigationExpanded: false,
                        /* polling frequency for the chat window */
                        chatPollFrequency: 5000,
                        /* maximum chat lines requested from server */
                        chatLineMaxlength: 200,
                        /* maximum length of avatar notes */
                        notesLineMaxlength: 200,
                        sessionDeprecated: 'Platnost vašeho připojení již vypršela, prosím přihlaste se znovu přes startovní stránku!',
                        /* the background data object, contains different data for city, island and worldmap. to be passed to the screen object in ikariam.BackgroundView */
                        bgViewData: bgViewData,
                        /* the island shortcuts for map navigation*/
                        shortcuts: islandShortcuts,
                        /* the last action request, used as one click token */
                        actionRequest: "f8f5f370134d46cb215c82f92fcee9a0",
                        /* some infos for displaying the title tag */
                        nextETA: 0,
                        breadcrumbsWorldlinkText: 'Svět',

                        animationsActive: "1",
                        /* add a feedback message directly after reload (advisor) */
                        feedbackMsg: feedbackMsg,
                        /* if the tutorial is displayed, provide an additional data array... */
                        tutorialData: tutorialData,
                        /* data for happy hour countdown and popup, if active */
                        happyHourConfig: happyHourConfig,
                        /* data for ingame countdown and popup, if active */
                        ingameCounterConfig: ingameCounterConfig,
                        /* die normalen view parameter, potentiell eines templateViews */
                        viewParams: JSON.parse('{\"pwat_uid\":\"\",\"pwat_checksum\":\"\",\"startPageShown\":\"1\",\"detectedDevice\":\"1\",\"autoLogin\":\"on\",\"cityId\":\"100470\"}'),
                        /* premium account active? */
                        hasPremiumAccount: '',
                        /* reset sliders on create view? (default: false [slider positions are stored], after actions: clear!) */
                        resetSliders: 0,
                        /* all relevant data to each related city (own, occupied, allied): the id, name, coords and produced tradegood. needed to update the global city dropdown menu. */
                        relatedCityData: JSON.parse('{\"city_100470\":{\"id\":100470,\"name\":\"Simplicity I\",\"coords\":\"[54:60] \",\"tradegood\":\"4\",\"relationship\":\"ownCity\"},\"city_101208\":{\"id\":101208,\"name\":\"Simplicity II\",\"coords\":\"[54:59] \",\"tradegood\":\"1\",\"relationship\":\"ownCity\"},\"city_101439\":{\"id\":101439,\"name\":\"Simplicity III\",\"coords\":\"[52:59] \",\"tradegood\":\"2\",\"relationship\":\"ownCity\"},\"city_103575\":{\"id\":103575,\"name\":\"Simplicity IV\",\"coords\":\"[52:58] \",\"tradegood\":\"3\",\"relationship\":\"ownCity\"},\"additionalInfo\":\"cityCoords\",\"selectedCity\":\"city_100470\"}'),
                        /* all relevant data to display the 4 advisors (highlights, alerts, links, premium ad) */
                        advisorData: JSON.parse('{\"military\":{\"link\":\"?view=militaryAdvisor&oldView=city&cityId=100470\",\"cssclass\":\"normal\",\"premiumlink\":\"?view=premiumDetails&oldView=city&cityId=100470\"},\"cities\":{\"cssclass\":\"normalactive\",\"link\":\"?view=tradeAdvisor&oldView=city&cityId=100470\",\"premiumlink\":\"?view=premiumDetails&oldView=city&cityId=100470\"},\"research\":{\"cssclass\":\"normal\",\"link\":\"?view=researchAdvisor&oldView=city&cityId=100470\",\"premiumlink\":\"?view=premiumDetails&oldView=city&cityId=100470\"},\"diplomacy\":{\"cssclass\":\"normal\",\"link\":\"?view=diplomacyAdvisor&oldView=city&cityId=100470\",\"premiumlink\":\"?view=premiumDetails&oldView=city&cityId=100470\"},\"hasPremiumAccount\":false}'),

                        isIPhone: 0,

                        touchVersion: 0,

                        lightVersion: 0,

                        heavyVersion: '',

                        mapNativeScrolling: 0,

                        mainbox_x: 0,

                        mainbox_y: 0,

                        mainbox_z: 0,

                        sidebar_x: 0,

                        sidebar_y: 0,

                        sidebar_z: 0,


                        cityParametersAlreadySet: 0,

                        islandParametersAlreadySet: 0,

                        worldview_scale_city: 1,

                        worldview_scale_island: 1,

                        netbarActive: 0,
                        worldview_scroll_left_city: 230,
                        worldview_scroll_top_city: 120,
                        worldview_scroll_left_island: 265,
                        worldview_scroll_top_island: 190,
                        specialEvent: 0

      };

    /**
     * INSTANTIATE FRONTEND
     * AND HANDLE WINDOW EVENTS ON PAGE
     */
    $(document).ready(init);

    /**
     * instantiate js-frontend (see ikariam.js)
     *
     * pass the php dataset to namespace ikariam.
     * on initialization, the main data model ikariam.Model, the background view ikariam.BackgroundView (holding city, island or worldmap screen)
     * and a controller ikariam.Controller for event handling and responding ajax requests is created.
     *
     */
    function init() {

        ikariam.init(dataSetForView);
         // shortcuts for some help functions, called in various templates.
        var model = ikariam.getModel();
        shortenValue = model.shortenValue;
        locaNumberFormat = model.locaNumberFormat;
        numberFormat = model.numberFormat;


        
        // city background: load construction list sidebar as template view, if activated
        if (ikariam.backgroundView.id === "city" && ikariam.getScreen().constructionListState > 0 ) {
                    }
        // island background: preselect city
                    else if (ikariam.backgroundView.id === "island") {
                ikariam.getScreen().preselectCity();
            }
            
        // load daily activity popup
        
         // load special promotion popup
                    ikariam.MultiPopupController.addTab("Využijte tuto jedinečnou nabídku!", '<center><a href="?view=premiumPayment&oldView=premium" target=""><img height="106" src="http://gf1.geo.gfsrv.net/cdn9a/ed32692ed753d61410fd044748a510.jpg" alt="" class="promoImage"/></a></center><p>Sluneční hodiny na Vašem tržišti to potvrzují, musíte si pospíšit! Šťastné hodiny začaly a rychle zase skončí. Zajistěte si rychle bonus 20 procent při nákupu ambrózií - teď během šťastných hodin na Ikariamu!</p><div class="centerButton"><a class="button" href="?view=premiumPayment&oldView=premium" onmousedown="ajaxHandlerCall(this.href); return false;">Získat Ambrózie</a></div>', '', 50);
                    ikariam.MultiPopupController.closeButtonText = 'Zavřít';
        ikariam.MultiPopupController.showPopup();

        /*
        if(touchVersion) {
            alert("pups");
            $(".a").on("touchstart",
                    function(e) {
                        e.preventDefault();
                        $(this).trigger($.extend(e, {
                            type: 'mousedown',
                            pageX: e.originalEvent.touches[0].pageX,
                            pageY: e.originalEvent.touches[0].pageY
                        }));
                    })
                    .on("touchmove",
                    function(e) {
                        e.preventDefault();
                        $(this).trigger($.extend(e, {
                            type: 'mousemove',
                            pageX: e.originalEvent.touches[0].pageX,
                            pageY: e.originalEvent.touches[0].pageY
                        }));
                    })
                    .on("touchend",
                    function(e) {
                        e.preventDefault();
                        $(this).trigger($.extend(e, {
                            type: 'mouseup'
                        }));
                    });
        }   */

    }

    init.periodicalDataRefresh = setInterval("ajaxHandlerCall('?view=updateGlobalData')", 60000);

    /**
     * on unload, store cookie data for avatar notes and chat window.
     */
    w.onunload = function() {
        $("#loadingPreview").css("display", "block");
        if (ikariam.getPopupController() !== null) ikariam.getPopupController().saveCookies();
    };
    $("#loadingPreview").css("display", "none");


</script>        <script type="text/javascript">
            $(document).ready(function() {
                ikariam.getClass(ajax.Responder, [["updateBackgroundData",{"name":"Simplicity I","id":"100470","phase":4,"ownerId":"50260","ownerName":"pickwick","islandId":"244","islandName":"Tejios","islandXCoord":"54","islandYCoord":"60","buildingSpeedupActive":1,"showPirateFortressBackground":1,"showPirateFortressShip":0,"lockedPosition":{"13":"Abyste mohli vyu\u017e\u00edt toto stavebn\u00ed m\u00edsto, mus\u00edte m\u00edt dokon\u010den v\u00fdzkum byrokracie"},"underConstruction":-1,"endUpgradeTime":-1,"startUpgradeTime":-1,"position":[{"name":"M\u011bstsk\u00e1 radnice","level":"17","isBusy":false,"building":"townHall"},{"name":"Obchodn\u00ed p\u0159\u00edstav","level":"10","isBusy":false,"building":"port"},{"name":"Lod\u011bnice","level":"6","isBusy":false,"building":"shipyard"},{"name":"Tesa\u0159ova d\u00edlna","level":"15","isBusy":false,"building":"carpentering"},{"name":"\u00dakryt","level":"8","isBusy":false,"building":"safehouse"},{"name":"Sklad","level":"14","isBusy":false,"building":"warehouse"},{"name":"Muzeum","level":"3","isBusy":false,"building":"museum"},{"name":"D\u00edlna","level":"8","isBusy":true,"building":"workshop"},{"name":"Chr\u00e1m","level":"3","isBusy":false,"building":"temple"},{"name":"Kas\u00e1rna","level":"12","isBusy":false,"building":"barracks"},{"name":"Pal\u00e1c","level":"4","isBusy":false,"building":"palace"},{"name":"Tr\u017ei\u0161t\u011b","level":"8","isBusy":false,"building":"branchOffice"},{"name":"Akademie","level":"11","isBusy":false,"building":"academy"},{"building":"buildingGround land"},{"name":"M\u011bstsk\u00e1 ze\u010f","level":"9","isBusy":false,"building":"wall"},{"building":"buildingGround land"},{"name":"Sklad","level":"14","isBusy":false,"building":"warehouse"},{"building":"buildingGround sea"},{"name":"Hostinec","level":"15","isBusy":false,"building":"tavern"}],"beachboys":"visible","spiesInside":null,"cityLeftMenu":{"visibility":{"military":1,"espionage":0,"resourceShop":1,"slot1":1,"slot2":1,"slot3":0,"slot4":1},"ownCity":1},"walkers":{"add":[["walkerAmbience5739","walkers",[[454,541,"none","310",1432565194,"citizen4"],[454,541,"SO","310",1432565194,"citizen4"],[490,560,"SO","310",1432565197,"citizen4"],[534,545,"O","310",1432565200,"citizen4"],[593,590,"SO","310",1432565205,"citizen4"],[631,618,"SO","310",1432565208,"citizen4"],[704,582,"NO","310",1432565213,"citizen4"],[761,555,"NO","310",1432565218,"citizen4"],[823,522,"NO","299",1432565222,"citizen4"],[861,505,"NO","299",1432565225,"citizen4"],[971,442,"NO","310",1432565233,"citizen4"],[1121,514,"SO","299",1432565245,"citizen4"],[1146,531,"SO","310",1432565247,"citizen4"],[1169,541,"SO","310",1432565248,"citizen4"],[1250,583,"SO","310",1432565254,"citizen4"],[1288,594,"O","310",1432565257,"citizen4"],[1300,584,"NO","310",1432565258,"citizen4"],[1288,594,"SW","310",1432565259,"citizen4"],[1250,583,"W","310",1432565262,"citizen4"],[1169,541,"NW","310",1432565268,"citizen4"],[1146,531,"NW","310",1432565269,"citizen4"],[1121,514,"NW","310",1432565271,"citizen4"],[1083.5,496,"NW","310",1432565274,"citizen4"],[971,442,"NW","299",1432565283,"citizen4"],[861,505,"SW","310",1432565291,"citizen4"],[823,522,"SW","299",1432565294,"citizen4"],[761,555,"SW","299",1432565298,"citizen4"],[704,582,"SW","310",1432565303,"citizen4"],[631,618,"SW","310",1432565308,"citizen4"],[593,590,"NW","310",1432565311,"citizen4"],[534,545,"NW","310",1432565316,"citizen4"],[490,560,"W","310",1432565319,"citizen4"],[454,541,"NW","310",1432565322,"citizen4"]],1432565274,"\u017divot je zde lep\u0161\u00ed ne\u017e v At\u00e9n\u00e1ch!",1432565322,0],["walkerAmbience5740","walkers",[[518,276,"none","310",1432565189,"citizen1"],[518,276,"SO","310",1432565189,"citizen1"],[544,290,"SO","310",1432565191,"citizen1"],[552,323,"S","299",1432565193,"citizen1"],[781,444,"SO","299",1432565211,"citizen1"],[823,522,"SO","310",1432565217,"citizen1"],[761,555,"SW","299",1432565221,"citizen1"],[704,582,"SW","310",1432565226,"citizen1"],[631,618,"SW","310",1432565231,"citizen1"],[593,590,"NW","310",1432565234,"citizen1"],[534,545,"NW","310",1432565239,"citizen1"],[490,560,"W","310",1432565242,"citizen1"],[454,541,"NW","310",1432565245,"citizen1"],[490,560,"SO","310",1432565248,"citizen1"],[534,545,"O","310",1432565251,"citizen1"],[593,590,"SO","310",1432565256,"citizen1"],[631,618,"SO","310",1432565259,"citizen1"],[704,582,"NO","310",1432565264,"citizen1"],[761,555,"NO","310",1432565269,"citizen1"],[823,522,"NO","299",1432565273,"citizen1"],[816,509,"NO","299",1432565274,"citizen1"],[781,444,"NW","310",1432565279,"citizen1"],[552,323,"NW","299",1432565297,"citizen1"],[544,290,"N","299",1432565299,"citizen1"],[518,276,"NW","310",1432565301,"citizen1"]],1432565274,"Miluji b\u011bh\u00e1n\u00ed!",1432565301,0],["walkerAmbience5743","walkers",[[518,276,"none","310",1432565196,"citizen1"],[518,276,"SO","310",1432565196,"citizen1"],[544,290,"SO","310",1432565198,"citizen1"],[552,323,"S","299",1432565200,"citizen1"],[781,444,"SO","299",1432565218,"citizen1"],[894,390,"NO","299",1432565226,"citizen1"],[931,406,"SO","299",1432565229,"citizen1"],[971,382,"NO","299",1432565232,"citizen1"],[1059,340,"NO","308",1432565238,"citizen1"],[1098,352,"O","308",1432565241,"citizen1"],[1126,339,"NO","310",1432565243,"citizen1"],[1172,379,"SO","310",1432565247,"citizen1"],[1126,339,"NW","310",1432565251,"citizen1"],[1098,352,"SW","310",1432565253,"citizen1"],[1059,340,"W","308",1432565256,"citizen1"],[971,382,"SW","308",1432565262,"citizen1"],[931,406,"SW","299",1432565265,"citizen1"],[894,390,"NW","299",1432565268,"citizen1"],[809.25,430.5,"NW","299",1432565274,"citizen1"],[781,444,"SW","299",1432565276,"citizen1"],[552,323,"NW","299",1432565294,"citizen1"],[544,290,"N","299",1432565296,"citizen1"],[518,276,"NW","310",1432565298,"citizen1"]],1432565274,"Pt\u00e1ci odlet\u011bli do hn\u00edzd\u2026 vypad\u00e1 to, \u017ee by mohlo pr\u0161et!",1432565298,0],["walkerAmbience5745","walkers",[[1298,582,"none","310",1432565195,"soldierOwn"],[1298,582,"SW","310",1432565195,"soldierOwn"],[1286,592,"SW","310",1432565196,"soldierOwn"],[1248,581,"W","310",1432565199,"soldierOwn"],[1167,539,"NW","310",1432565205,"soldierOwn"],[1144,529,"NW","310",1432565207,"soldierOwn"],[1119,512,"NW","310",1432565209,"soldierOwn"],[969,440,"NW","299",1432565220,"soldierOwn"],[929,404,"NW","299",1432565223,"soldierOwn"],[969,380,"NO","299",1432565227,"soldierOwn"],[1057,338,"NO","308",1432565233,"soldierOwn"],[1096,350,"O","308",1432565236,"soldierOwn"],[1124,337,"NO","310",1432565238,"soldierOwn"],[1170,377,"SO","310",1432565242,"soldierOwn"],[1124,337,"NW","310",1432565246,"soldierOwn"],[1096,350,"SW","310",1432565248,"soldierOwn"],[1057,338,"W","308",1432565251,"soldierOwn"],[969,380,"SW","308",1432565257,"soldierOwn"],[929,404,"SW","299",1432565261,"soldierOwn"],[969,440,"SO","299",1432565264,"soldierOwn"],[1105.3636363636,505.45454545455,"SO","299",1432565274,"soldierOwn"],[1119,512,"SO","299",1432565275,"soldierOwn"],[1144,529,"SO","310",1432565277,"soldierOwn"],[1167,539,"SO","310",1432565279,"soldierOwn"],[1248,581,"SO","310",1432565285,"soldierOwn"],[1286,592,"O","310",1432565288,"soldierOwn"],[1298,582,"NO","310",1432565289,"soldierOwn"]],1432565274,"Mus\u00edm zase brzy nav\u0161t\u00edvit sv\u00e9 p\u0159\u00edbuzn\u00e9!",1432565289,0],["walkerAmbience5746","walkers",[[1016,418,"none","310",1432565193,"citizen4"],[1016,418,"NW","310",1432565193,"citizen4"],[971,382,"NW","310",1432565197,"citizen4"],[931,406,"SW","299",1432565200,"citizen4"],[971,442,"SO","299",1432565204,"citizen4"],[861,505,"SW","310",1432565212,"citizen4"],[823,522,"SW","299",1432565215,"citizen4"],[761,555,"SW","299",1432565219,"citizen4"],[704,582,"SW","310",1432565224,"citizen4"],[631,618,"SW","310",1432565229,"citizen4"],[593,590,"NW","310",1432565232,"citizen4"],[534,545,"NW","310",1432565237,"citizen4"],[490,560,"W","310",1432565240,"citizen4"],[454,541,"NW","310",1432565243,"citizen4"],[490,560,"SO","310",1432565246,"citizen4"],[534,545,"O","310",1432565249,"citizen4"],[593,590,"SO","310",1432565254,"citizen4"],[631,618,"SO","310",1432565257,"citizen4"],[704,582,"NO","310",1432565262,"citizen4"],[761,555,"NO","310",1432565267,"citizen4"],[823,522,"NO","299",1432565271,"citizen4"],[861,505,"NO","299",1432565274,"citizen4"],[861,505,"NO","299",1432565274,"citizen4"],[971,442,"NO","310",1432565282,"citizen4"],[931,406,"NW","299",1432565286,"citizen4"],[971,382,"NO","299",1432565289,"citizen4"],[1016,418,"SO","310",1432565293,"citizen4"]],1432565274,"Poj\u010fme si poslechnout nejnov\u011bj\u0161\u00ed drby!",1432565293,0],["walkerProduction_resource_100470","walkers",[[112,-49,"none","299",1432565102,"resourceWalker"],[112,-49,"SO","299",1432565102,"resourceWalker"],[163,13,"SO","299",1432565107,"resourceWalker"],[241,59,"SO","299",1432565113,"resourceWalker"],[267,105,"SO","299",1432565117,"resourceWalker"],[314,128,"SO","299",1432565120,"resourceWalker"],[407,183,"SO","299",1432565128,"resourceWalker"],[430,230,"SO","299",1432565131,"resourceWalker"],[487,261,"SO","299",1432565135,"resourceWalker"],[514,276,"SO","310",1432565138,"resourceWalker"],[540,290,"SO","310",1432565139,"resourceWalker"],[548,323,"S","299",1432565142,"resourceWalker"],[777,444,"SO","299",1432565159,"resourceWalker"],[857,462,"O","310",1432565165,"resourceWalker"],[777,444,"W","310",1432565170,"resourceWalkerEmpty"],[548,323,"NW","299",1432565187,"resourceWalkerEmpty"],[540,290,"N","299",1432565190,"resourceWalkerEmpty"],[514,276,"NW","310",1432565191,"resourceWalkerEmpty"],[487,261,"NW","310",1432565194,"resourceWalkerEmpty"],[430,230,"NW","299",1432565198,"resourceWalkerEmpty"],[407,183,"NW","299",1432565201,"resourceWalkerEmpty"],[314,128,"NW","299",1432565209,"resourceWalkerEmpty"],[267,105,"NW","299",1432565212,"resourceWalkerEmpty"],[241,59,"NW","299",1432565216,"resourceWalkerEmpty"],[163,13,"NW","299",1432565222,"resourceWalkerEmpty"],[112,-49,"NW","299",1432565227,"resourceWalkerEmpty"],[112,-49,"none","299",1432565274,"resourceWalkerEmpty"],[112,-49,"none","299",1432565323,"resourceWalkerEmpty"]],1432565274,"Co vlastn\u011b starosta se v\u0161emi t\u011bmito surovinami d\u011bl\u00e1?<br>Produkce: <img src=\"skin\/resources\/icon_wood_small.png\" \/> +200 za Hodinu",9223372036854775807,0]]}}],["updateTemplateData",""],["popupData",null],["ingameCounterData",null],["removeIngameCounterData",null],["updateBacklink",null]]);
            });
        </script>
    </body>
</html>