<?php namespace Ikariam\Util;

/**
 * Class Time
 */
class TimeFormat
{
	const WEEK = 604800;
	const DAY = 86400;
	const HOUR = 3600;
	const MINUTE = 60;
	const SECOND = 1;
	const MILISECOND = 0.001;

	/**
	 * @var array
	 */
	protected $units = [
		'w'  => self::WEEK,
		'd'  => self::DAY,
		'h'  => self::HOUR,
		'm'  => self::MINUTE,
		's'  => self::SECOND,
		'ms' => self::MILISECOND
	];



	/**
	 * Vrací čas ve formátu
	 * [
	 *        'h' => 1,
	 *    's' => 2
	 * ]
	 *
	 * @param $seconds
	 *
	 * @return array
	 */
	public function getUnits($seconds)
	{
		// m => 1, s => 2
		$units = [];

		// m => 60, s => 1
		foreach ($this->units as $unit => $amount)
		{
			// 601 -> 10 * 60 (m) zůstane sekunda
			$diff = floor($seconds / $amount);

			// pokud tam nebyl rok y=>0, tak nezapisovat
			if ($diff != 0)
			{
				$units[$unit] = $diff;
			}

			// odečtu, aby nevznikalo
			// 3600 -> 1h, 60m, 3600s, ....
			$seconds -= $diff * $amount;
		}

		return $units;
	}



	/**
	 * 3m 2s
	 *
	 * @param $seconds
	 *
	 * @return string
	 */
	public function getInUnits($seconds)
	{
		$text = '';

		foreach ($this->getUnits($seconds) as $unit => $amount)
		{
			$text .= $amount . $unit . ' ';
		}

		return rtrim($text, ' ');
	}
}