<?php namespace Ikariam\Entity;

use Ikariam\Enum\City as CityType;
use Ikariam\Enum\Resource;

/**
 * Entity of city
 * @package Ikariam\Entity
 */
class City
{

	/**
	 * Properties of the city
	 * @var array
	 */
	private $atts = [
		'id'                  => 0,
		'name'                => '',
		'coords'              => [
			'x' => 0,
			'y' => 0
		],
		'type'                => CityType::OWN,
		'resources'           => [
			Resource::WOOD          => [
				'current' => 0,
				'max'     => 0,
			],
			Resource::MARBLE        => [
				'current' => 0,
				'max'     => 0,
			],
			Resource::WINE          => [
				'current' => 0,
				'max'     => 0,
			],
			Resource::SULFUR        => [
				'current' => 0,
				'max'     => 0,
			],
			Resource::CRYSTAL_GLASS => [
				'current' => 0,
				'max'     => 0,
			],
		],
		'resource'            => null,
		'population'          => null,
		'citizens'            => null,
		'wine_spending'       => null,
		'resourceProduction'  => null,
		'tradegoodProduction' => null,
		'islandId'            => null,
	];



	/**
	 * @param $name
	 *
	 * @return $this
	 */
	public function setName($name)
	{
		$this->atts['name'] = $name;

		return $this;
	}



	/**
	 * @param $coords
	 *
	 * @return $this
	 */
	public function setCoords($coords)
	{
		$this->atts['coords'] = $coords;

		return $this;
	}



	/**
	 * @param $resources
	 *
	 * @return $this
	 */
	public function setResources($resources)
	{
		$this->atts['resources'] = $resources;

		return $this;
	}



	/**
	 * @param $population
	 *
	 * @return $this
	 */
	public function setPopulation($population)
	{
		$this->atts['population'] = $population;

		return $this;
	}



	/**
	 * @return mixed
	 */
	public function getName()
	{
		return $this->atts['name'];
	}



	/**
	 * @return mixed
	 */
	public function getCoords()
	{
		return $this->atts['coords'];
	}



	/**
	 * @param $id
	 *
	 * @return $this
	 */
	public function setId($id)
	{
		$this->atts['id'] = $id;

		return $this;
	}



	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->atts['id'];
	}



	/**
	 * @param $resource
	 *
	 * @return $this
	 */
	public function setExtraResource($resource)
	{
		$this->atts['resource'] = $resource;

		return $this;
	}



	/**
	 * @return mixed
	 */
	public function getExtraResource()
	{
		return $this->atts['resource'];
	}



	/**
	 * @param null $resource
	 *
	 * @return string
	 */
	public function getResourceText($resource = null)
	{
		if (is_null($resource))
		{
			$resource = $this->getExtraResource();
		}

		switch ($resource)
		{
			case Resource::WOOD:
				return 'WOOD';
			case Resource::WINE:
				return 'WINE';
			case Resource::MARBLE:
				return 'MARBLE';
			case Resource::SULFUR:
				return 'SULFUR';
			case Resource::CRYSTAL_GLASS:
				return 'CRYSTAL GLASS';
		}

		return 'UKNOWN';
	}



	/**
	 * @param $citizens
	 *
	 * @return $this
	 */
	public function setCitizens($citizens)
	{
		$this->atts['citizens'] = (int)$citizens;

		return $this;
	}



	/**
	 * @return mixed
	 */
	public function getCitizens()
	{
		return $this->atts['citizens'];
	}



	/**
	 * @return mixed
	 */
	public function getResources()
	{
		return $this->atts['resources'];
	}



	/**
	 * @return mixed
	 */
	public function getPopulation()
	{
		return $this->atts['population'];
	}



	/**
	 * @return mixed
	 */
	public function getWineSpending()
	{
		return $this->atts['wine_spending'];
	}



	/**
	 * @param $wine
	 *
	 * @return $this
	 */
	public function setWineSpending($wine)
	{
		$this->atts['wine_spending'] = $wine;

		return $this;
	}



	/**
	 * @return mixed
	 */
	public function getResourceProduction()
	{
		return $this->atts['resourceProduction'];
	}



	/**
	 * @param $resource
	 *
	 * @return $this
	 */
	public function setResourceProduction($resource)
	{
		$this->atts['resourceProduction'] = $resource;

		return $this;
	}



	/**
	 * @return mixed
	 */
	public function getTradegoodProduction()
	{
		return $this->atts['tradegoodProduction'];
	}



	/**
	 * @param $resource
	 *
	 * @return $this
	 */
	public function setTradegoodProduction($resource)
	{
		$this->atts['tradegoodProduction'] = $resource;

		return $this;
	}



	/**
	 * @param $id
	 *
	 * @return $this
	 */
	public function setIslandId($id)
	{
		$this->atts['islandId'] = $id;

		return $this;
	}



	/**
	 * @return mixed
	 */
	public function getIslandId()
	{
		return $this->atts['islandId'];
	}
}