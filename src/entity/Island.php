<?php namespace Ikariam\Entity;

use Ikariam\Enum\Resource;

/**
 * Entity of island
 * @package Ikariam\Entity
 */
class Island
{

	/**
	 * Properties of island.
	 * @var array
	 */
	private $atts = [
		'name'   => '',
		'coords' => [
			'x' => 0,
			'y' => 0
		],
	];



	/**
	 * @param $name
	 *
	 * @return $this
	 */
	public function setName($name)
	{
		$this->atts['name'] = $name;

		return $this;
	}



	/**
	 * @param $coords
	 *
	 * @return $this
	 */
	public function setCoords($coords)
	{
		$this->atts['coords'] = $coords;

		return $this;
	}



	/**
	 * @param $id
	 *
	 * @return $this
	 */
	public function setId($id)
	{
		$this->atts['id'] = $id;

		return $this;
	}



	/**
	 * @param $id
	 *
	 * @return $this
	 */
	public function setExtraResource($id)
	{
		$this->atts['resource'] = $id;

		return $this;
	}



	/**
	 * @param $count
	 *
	 * @return $this
	 */
	public function setCityCount($count)
	{
		$this->atts['city_count'] = $count;

		return $this;
	}



	/**
	 * @param $level
	 *
	 * @return $this
	 */
	public function setLumberLevel($level)
	{
		$this->atts['lumber_level'] = $level;

		return $this;
	}



	/**
	 * @return mixed
	 */
	public function getName()
	{
		return $this->atts['name'];
	}



	/**
	 * @return mixed
	 */
	public function getCoords()
	{
		return $this->atts['coords'];
	}



	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->atts['id'];
	}



	/**
	 * @return mixed
	 */
	public function getCityCount()
	{
		return $this->atts['city_count'];
	}



	/**
	 * @param null $resource
	 *
	 * @return string
	 */
	public function getExtraResource($resource = null)
	{
		if (is_null($resource))
		{
			$resource = $this->atts['resource'];
		}

		switch ($resource)
		{
			case Resource::WOOD:
				return 'WOOD';
			case Resource::WINE:
				return 'WINE';
			case Resource::MARBLE:
				return 'MARBLE';
			case Resource::SULFUR:
				return 'SULFUR';
			case Resource::CRYSTAL_GLASS:
				return 'CRYSTAL GLASS';
		}

		return 'UKNOWN';
	}
}