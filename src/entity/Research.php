<?php namespace Ikariam\Entity;

/**
 * Abstract entity of research
 * @package Ikariam\Entity
 */
abstract class Research
{
	/**
	 * @return string
	 */
	abstract public function getName();



	/**
	 * @return int
	 */
	abstract public function getWoodCost();



	/**
	 * @return int
	 */
	abstract public function getWineCost();



	/**
	 * @return int
	 */
	abstract public function getMarbleCost();



	/**
	 * @return int
	 */
	abstract public function getSulfurCost();



	/**
	 * @return int
	 */
	abstract public function getCrystalCost();
}