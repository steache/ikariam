<?php //namespace Ikariam;
//
//use Pick\Config\Configuration;
//
///**
// * Class Linker
// * @package Ikariam
// */
//class Linker
//{
//
//	/**
//	 * @var Configuration
//	 */
//	private $config;
//
//	/**
//	 * @param Configuration $config
//	 */
//	public function __construct(Configuration $config)
//	{
//		$this->config = $config;
//	}
//
//	/**
//	 * @return string
//	 */
//	public function getLoginUrl()
//	{
//		return $this->getBaseUrl() . $this->config->get('ikariam.login.url');
//	}
//
//	/**
//	 * @return mixed
//	 */
//	private function getBaseUrl()
//	{
//		$base = $this->config->get('ikariam.base-url');
//
//		return str_replace('[SERVER]', $this->config->get('ikariam.server'), $base);
//	}
//
//	/**
//	 * @return mixed
//	 */
//	public function getCityInfoUrl()
//	{
//		return $this->getBaseUrl();
//	}
//}