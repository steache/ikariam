<?php namespace Ikariam;

use DOMDocument;
use Ikariam\Manager\Request;

/**
 * Class Login
 * @package Ikariam
 */
class Login extends Request
{

	/**
	 * @var string
	 */
	private $requestUrl = 'http://[SERVER]/index.php?action=loginAvatar&function=login';

	/**
	 * @var string
	 */
	private $requestData = 'uni_url=[SERVER]&name=[USERNAME]&password=[PASSWORD]&pwat_uid=&pwat_checksum=&startPageShown=1&detectedDevice=1&kid=&autoLogin=on';



	/**
	 * @param array $atts
	 */
	public function __construct($atts = [])
	{
		$atts['user_agent'] =
			'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.152 Safari/537.36';

		parent::__construct($atts);
	}



	/**
	 * @param array $atts
	 * @param bool  $choose
	 *
	 * @return mixed
	 */
	public function make($atts = [], $choose = false)
	{
		$this->atts = array_merge($this->atts, $atts);

		$url  = $this->buildRequestUrl($this->atts);
		$data = $this->buildRequestData($this->atts);

		$cookiePath = realpath($this->atts['cachePath']) . DIRECTORY_SEPARATOR . 'cookie.txt';

		$fp = fopen($cookiePath, 'w');
		fclose($fp);

		$login = curl_init();
		curl_setopt($login, CURLOPT_COOKIEJAR, $cookiePath);
		curl_setopt($login, CURLOPT_COOKIEFILE, $cookiePath);
		curl_setopt($login, CURLOPT_TIMEOUT, 40000);
		curl_setopt($login, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($login, CURLOPT_URL, $url);
		curl_setopt($login, CURLOPT_USERAGENT, $this->atts['user_agent']);
		curl_setopt($login, CURLOPT_POST, true);
		curl_setopt($login, CURLOPT_POSTFIELDS, $data);
		ob_start();

		$response = curl_exec($login);

		file_put_contents(PATH . 'login-response.php', print_r($response, true));

		$dom = new DOMDocument();

		@$dom->loadHTML($response);

		foreach ($dom->getElementsByTagName('input') as $div)
		{
			$id = $div->getAttribute('id');

			if($id == 'js_ChangeCityActionRequest')
			{
				var_dump('setting token ;)');

				IkariamStatic::$token = $div->getAttribute('value');
			}
		}

		return $response;
	}



	/**
	 * @param $atts
	 *
	 * @return string
	 */
	private function buildRequestUrl($atts)
	{
		return strtr($this->requestUrl, $atts);
	}



	/**
	 * @param $atts
	 *
	 * @return string
	 */
	private function buildRequestData($atts)
	{
		return strtr($this->requestData, $atts);
	}
}