<?php //namespace Ikariam;
//
//use Pick\Filesystem\FileManager;
//
//class Request
//{
//
//	/**
//	 * @var FileManager
//	 */
//	private $fileManager;
//
//	/**
//	 * @param FileManager $fileManager
//	 */
//	public function __construct(FileManager $fileManager)
//	{
//		$this->fileManager = $fileManager;
//		$this->fileManager->setBasePath(TEMP_PATH);
//	}
//
//	/**
//	 * @param $url
//	 * @param $data
//	 * @return mixed
//	 */
//	public function login($url, $data)
//	{
//		$fp = fopen($this->fileManager->getBasePath() . "cookie.txt", "w");
//		fclose($fp);
//		$login = curl_init();
//		curl_setopt($login, CURLOPT_COOKIEJAR, "cookie.txt");
//		curl_setopt($login, CURLOPT_COOKIEFILE, "cookie.txt");
//		curl_setopt($login, CURLOPT_TIMEOUT, 40000);
//		curl_setopt($login, CURLOPT_RETURNTRANSFER, true);
//		curl_setopt($login, CURLOPT_URL, $url);
//		curl_setopt($login, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
//		curl_setopt($login, CURLOPT_POST, true);
//		curl_setopt($login, CURLOPT_POSTFIELDS, $data);
//		ob_start();
//
//		return curl_exec($login);
//	}
//
//	/**
//	 * @param $site
//	 * @return mixed
//	 */
//	public function grab_page($site)
//	{
//		$ch = curl_init();
//		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//		curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
//		curl_setopt($ch, CURLOPT_TIMEOUT, 40);
//		curl_setopt($ch, CURLOPT_COOKIEFILE, "cookie.txt");
//		curl_setopt($ch, CURLOPT_URL, $site);
//		ob_start();
//
//		return curl_exec($ch);
//	}
//
//	/**
//	 * @param $site
//	 * @param $data
//	 * @return string
//	 */
//	public function post_data($site, $data)
//	{
//		$datapost = curl_init();
//		$headers = array ("Expect:");
//		curl_setopt($datapost, CURLOPT_URL, $site);
//		curl_setopt($datapost, CURLOPT_TIMEOUT, 40000);
//		curl_setopt($datapost, CURLOPT_HEADER, true);
//		curl_setopt($datapost, CURLOPT_HTTPHEADER, $headers);
//		curl_setopt($datapost, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
//		curl_setopt($datapost, CURLOPT_POST, true);
//		curl_setopt($datapost, CURLOPT_POSTFIELDS, $data);
//		curl_setopt($datapost, CURLOPT_COOKIEFILE, "cookie.txt");
//		ob_start();
//		$result = curl_exec($datapost);
//
//		return ob_get_clean();
//	}
//}