<?php //namespace Ikariam;
//use Pick\Config\Configuration;
//
///**
// * Class Api
// * @package Ikariam
// */
//class Api
//{
//
//	/**
//	 * @var Request
//	 */
//	private $request;
//
//	/**
//	 * @var Linker
//	 */
//	private $linker;
//
//	/**
//	 * @var \Pick\Config\Configuration
//	 */
//	private $config;
//
//	/**
//	 * @param Request                    $request
//	 * @param Linker                     $linker
//	 * @param \Pick\Config\Configuration $config
//	 */
//	public function __construct(Request $request, Linker $linker, Configuration $config)
//	{
//		$this->request = $request;
//		$this->linker = $linker;
//		$this->config = $config;
//	}
//
//	/**
//	 *
//	 */
//	public function login()
//	{
//		$url = $this->linker->getLoginUrl();
//		$data = [
//			'[SERVER]' => $this->c('server'),
//			'[USERNAME]' => $this->c('username'),
//			'[PASSWORD]' => $this->c('password'),
//		];
//
//		$this->request->login($url, strtr($this->c('login.data'), $data));
//	}
//
//	/**
//	 * @param $key
//	 * @return mixed|null
//	 */
//	public function c($key)
//	{
//		return $this->config->get('ikariam.'.$key);
//	}
//
//
//
//	/**
//	 * @param null $id
//	 *
//	 * @return array|mixed|string
//	 */
//	public function getInfo($id = null)
//	{
//		$url = $this->linker->getCityInfoUrl();
//
//		$id = is_null($id)
//			? $this->config->get('city-id')
//			: $id;
//
//		$data = [
//			'[CITY_ID]' => $id
//		];
//
//		$data = strtr($this->c('city.data'), $data);
//
//		$response = $this->request->post_data($url, $data);
//
//		$response = explode("\n", $response);
//
//		@$response = json_decode($response[12]);
//
//		return $this->object_to_array($response)[0][1];
//	}
//
//	public function object_to_array($obj) {
//		if(is_object($obj)) $obj = (array) $obj;
//		if(is_array($obj)) {
//			$new = array();
//			foreach($obj as $key => $val) {
//				$new[$key] = $this->object_to_array($val);
//			}
//		}
//		else $new = $obj;
//		return $new;
//	}
//}