<?php namespace Ikariam\Manager;

use Ikariam\Entity\City as CityEntity;
use Ikariam\Enum\Resource;
use Ikariam\IkariamStatic;

/**
 * Transport manager
 * @package Ikariam\Manager
 */
class Transport
{

	/**
	 * @var Request
	 */
	private $request;



	/**
	 * @param Request $request
	 */
	public function __construct(Request $request)
	{
		$this->request = $request;
	}



	/**
	 * @param CityEntity $fromCity
	 * @param CityEntity $toCity
	 * @param array      $resources
	 *
	 * @return bool
	 */
	public function transport(CityEntity $fromCity, CityEntity $toCity, $resources = [])
	{
		$transporters = 0;

		foreach ($resources as $resource)
		{
			$transporters += round($resource / 500);
		}

		/*
		 * 'action' => 'transportOperations',
		'function' => 'loadTransportersWithFreight',
		'actionRequest' => TRUE,
		'destinationCityId' => $toCity,
		'id' => $toIsland,
		'cargo_resource'   => $resources['wood'],
		'cargo_tradegood1' => $resources['wine'],
		'cargo_tradegood2' => $resources['marble'],
		'cargo_tradegood3' => $resources['glass'],
		'cargo_tradegood4' => $resources['sulfur'],
		'transporters'     => $transports
		 */

		$atts = [
			'action'             => 'transportOperations',
			'function'           => 'loadTransportersWithFreight',
			'destinationCityId'  => $toCity->getId(),
			'islandId'           => $toCity->getIslandId(),
			'oldView'            => '',
			'position'           => '',
			'avatar2Name'        => '',
			'city2Name'          => '',
			'type'               => '',
			'activeTab'          => '',
			'premiumTransporter' => 0,
			'minusPlusValue'     => 500,
			'cargo_resource'     => isset($resources[Resource::WOOD]) ? $resources[Resource::WOOD] : 0,
			'capacity'           => 5,
			'max_capacity'       => 5,
			'jetPropulsion'      => 0,
			'transporters'       => $transporters,
			'backgroundView'     => 'city',
			'currentCityId'      => $fromCity->getId(),
			'templateView'       => 'transport',
			'currentTab'         => 'tabSendTransporter',
			'actionRequest'      => $this->request->atts['actionRequest'], //IkariamStatic::$token
			'ajax'               => 1
		];

//		var_dump($atts);
//
//		die();

		$rss = [
			Resource::WINE,
			Resource::MARBLE,
			Resource::CRYSTAL_GLASS,
			Resource::SULFUR
		];

		foreach ($rss as $rs)
		{
			$atts['cargo_tradegood' . $rs] = isset($resources[$rs]) ? $resources[$rs] : 0;
		}

		$data = http_build_query($atts);

		$attz         = $this->request->atts;
		$attz['data'] = $data;

		// doplním tam ten server
		$attz['url'] = strtr('http://[SERVER]/index.php', $attz);

		@$response = $this->request->make($attz, true);

		return $response != false;
	}
}