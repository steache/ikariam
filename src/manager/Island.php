<?php namespace Ikariam\Manager;

use Ikariam\Entity\Island as IslandEntity;
use Ikariam\IkariamException;

/**
 * Island manager
 * @package Ikariam
 */
class Island
{

	/**
	 * @var string
	 */
	private $url = 'http://[SERVER]/?action=WorldMap&function=getJSONArea&x_min=[XMIN]&x_max=[XMAX]&y_min=[YMIN]&y_max=[YMAX]';

	/**
	 * @var Request
	 */
	private $request;



	/**
	 * @param Request $request
	 */
	public function __construct(Request $request)
	{
		$this->request = $request;
	}



	/**
	 * @param $id
	 *
	 * @return IslandEntity
	 */
	public function getById($id)
	{
		$atts = [
			'action' => 'requestIsland',
			'cityId' => $id,
		];

		$response = $this->request->make($atts);

		return (new IslandEntity())
			->setName($response['headerData']['name'])
			->setCoords($response['headerData']['coords']);
	}



	/**
	 * @return IslandEntity[]
	 */
	public function getIslands()
	{
		return $this->getByArea(-500, 500, -500, 500);
	}



	/**
	 * @param $x
	 * @param $y
	 *
	 * @return IslandEntity
	 * @throws IkariamException
	 */
	public function getByCoords($x, $y)
	{
		$islands = $this->getByArea($x, $x, $y, $y);

		if (empty($islands))
		{
			throw new IkariamException('Island [' . $x . ':' . $y . '] not found.');
		}

		return $islands[0];
	}



	/**
	 * @param $xmin
	 * @param $xmax
	 * @param $ymin
	 * @param $ymax
	 *
	 * @return IslandEntity[]
	 */
	public function getByArea($xmin, $xmax, $ymin, $ymax)
	{
		$atts = [
			'[XMIN]' => $xmin,
			'[XMAX]' => $xmax,
			'[YMIN]' => $ymin,
			'[YMAX]' => $ymax,
		];

		$atts['data'] = $this->buildData($atts);

		$atts = array_merge($this->request->atts, $atts);

		$atts['url'] = strtr($this->url, $atts);

		// [0]request,[1]response
		$response = $this->request->make($atts, true)['data'];

		$islands = [];

		foreach ($response as $x => $ys)
		{
			foreach ($ys as $y => $island)
			{
				$islands[] = (new IslandEntity())
					->setId($island[0])
					->setName($island[1])
					->setCoords(['x' => $x, 'y' => $y])
					->setExtraResource($island[2])
					->setCityCount($island[7])
					->setLumberLevel($island[6]);
			}
		}

		return $islands;
	}



	/**
	 * @param $atts
	 *
	 * @return string
	 */
	private function buildData($atts)
	{
		return strtr('action=WorldMap&function=getJSONArea&x_min=[XMIN]&x_max=[XMAX]&y_min=[YMIN]&y_max=[YMAX]', $atts);
	}
}