<?php namespace Ikariam\Manager;

use Ikariam\IkariamException;
use Ikariam\Login;
use phpDocumentor\Reflection\DocBlock\Tag\VarTag;

/**
 * Class Request
 * @package Ikariam\Manager
 */
class Request
{

	/**
	 * @var array
	 */
	public $atts = [];



	/**
	 * @param array $atts
	 */
	public function __construct($atts = [])
	{
		$atts['user_agent'] =
			'Mozilla/5.0 (Windows NT 6.3; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.152 Safari/537.36';

		$this->atts = $atts;
	}



	/**
	 * @param array $atts
	 * @param bool  $choose
	 *
	 * @return array|mixed|string
	 */
	public function make($atts = [], $choose = false)
	{
		$atts = array_merge($this->getBaseAtts(), $atts);

		$cookiePath = realpath($this->atts['cachePath']) . DIRECTORY_SEPARATOR . 'cookie.txt';

		$datapost = curl_init();
		$headers  = array("Expect:");
		curl_setopt($datapost, CURLOPT_URL, $atts['url']);
		curl_setopt($datapost, CURLOPT_TIMEOUT, 40000);
		curl_setopt($datapost, CURLOPT_HEADER, true);
		curl_setopt($datapost, CURLOPT_HTTPHEADER, $headers);
		curl_setopt($datapost, CURLOPT_USERAGENT, $atts['user_agent']);
		curl_setopt($datapost, CURLOPT_POST, true);
		curl_setopt($datapost, CURLOPT_POSTFIELDS, $atts['data']);
		curl_setopt($datapost, CURLOPT_COOKIEFILE, $cookiePath);

		ob_start();

		curl_exec($datapost);

		$response = ob_get_clean();

		$response = explode("\n", $response);

		@$response = json_decode($response[12]);

		@$response = $this->objectToArray($response);

		if(isset($response[2][1][0]['text']))
		{
			if(!is_numeric(strpos($response[2][1][0]['text'], 'Upozornění')))
			{
				throw new IkariamException($response[2][1][0]['text']);
			}
		}

		if (isset($response[0][1]['actionRequest']))
		{
			$this->atts['actionRequest'] = $response[0][1]['actionRequest'];
		}

		if (!$choose)
		{
			return $response[0][1];
		}

		return $response;
	}



	/**
	 * @param $obj
	 *
	 * @return array
	 */
	private function objectToArray($obj)
	{
		if (is_object($obj))
		{
			$obj = (array)$obj;
		}

		if (is_array($obj))
		{
			$new = array();
			foreach ($obj as $key => $val)
			{
				$new[$key] = $this->objectToArray($val);
			}
		}
		else
		{
			$new = $obj;
		}

		return $new;
	}



	/**
	 * @return array
	 */
	private function getBaseAtts()
	{
		return $this->atts;
	}



	/**
	 * @throws IkariamException
	 */
	public function login()
	{
		$request = new Login($this->atts);

		$response = $request->make();

		if ($response === false)
		{
			throw new IkariamException('Could not do login, wrong username+password');
		}
	}
}