<?php namespace Ikariam\Manager;

use Ikariam\Entity\City as CityEntity;
use Ikariam\Enum\Resource;

/**
 * Manager of cities
 * @package Ikariam
 */
class City
{

	/**
	 * @var string
	 */
	private $cityRequestUrl = 'http://[SERVER]/';

	/**
	 * @var string
	 */
	private $cityData = 'action=header&function=changeCurrentCity&isMission=1&actionRequest=ddbc4503ec1faad47af9035d85098400&oldView=city&cityId=[CITY_ID]&tryAutoLogin=1&sideBarExt=0130e6f393fba4a5159e3ee1e4225052405f9a47e8f&backgroundView=city&currentCityId=[CITY_ID]&ajax=1';

	/**
	 * @var Request
	 */
	private $request;



	/**
	 * @param Request $request
	 */
	public function __construct(Request $request)
	{
		$this->request = $request;
	}



//	/**
//	 * @param $id
//	 *
//	 * @return CityEntity
//	 */
//	public function getById($id)
//	{
//		$atts = [
//			'action' => 'requestCity',
//		];
//
//		$response = $this->request->make($atts);
//
//		$resources = [
//			Resource::WOOD          => $response['resources']['wood'],
//			Resource::MARBLE        => $response['resources']['marble'],
//			Resource::SULFUR        => $response['resources']['sulfur'],
//			Resource::CRYSTAL_GLASS => $response['resources']['crystal'],
//			Resource::WINE          => $response['resources']['wine'],
//		];
//
//		return (new CityEntity())
//			->setName($response['headerData']['name'])
//			->setCoords($response['headerData']['coords'])
//			->setResources($resources)
//			->setPopulation($response['headerData']['population']);
//	}

	/**
	 * @return CityEntity[]
	 */
	public function getCities()
	{
		$atts = $this->request->atts;

		$atts = array_merge($this->request->atts, [
			'url'  => $this->getCityUrl($atts),
			'data' => $this->getCityData($atts),
		]);

		$response = $this->request->make($atts);

		$headerData       = $response['headerData'];
		$cityDropdownMenu = $headerData['cityDropdownMenu'];

		$cities = [];

		foreach ($cityDropdownMenu as $key => $value)
		{
			if (!isset($value['coords']))
			{
				continue;
			}

			list($x, $y) = explode(':', $value['coords']);

			$x = trim($x, '[');
			$y = trim($y, '] ');

			$cities[] = (new CityEntity())
				->setId($value['id'])
				->setName($value['name'])
				->setExtraResource($value['tradegood'])
				->setCoords(['x' => $x, 'y' => $y]);
		}

		return $cities;
	}



	/**
	 * @param $atts
	 *
	 * @return string
	 */
	private function getCityUrl($atts)
	{
		return strtr($this->cityRequestUrl, $atts);
	}



	/**
	 * @param $atts
	 *
	 * @return string
	 */
	private function getCityData($atts)
	{
		return strtr($this->cityData, $atts);
	}



	/**
	 * @param $id
	 *
	 * @return CityEntity
	 */
	public function getCityById($id)
	{
		$atts = array_merge($this->request->atts, [
			'[CITY_ID]' => (int)$id,
		]);

		$atts = array_merge($atts, [
			'url'       => $this->getCityUrl($atts),
			'data'      => $this->getCityData($atts),
			'[CITY_ID]' => (int)$id
		]);

		$response = $this->request->make($atts);

		$data         = $response['backgroundData'];
		$headerData   = $response['headerData'];
		$curResources = $headerData['currentResources'];
		$maxResources = $headerData['maxResources'];

		$resources = [
			Resource::WOOD          => [
				'current' => $curResources['resource'],
				'max'     => $maxResources['resource']
			],
			Resource::MARBLE        => [
				'current' => $curResources[Resource::MARBLE],
				'max'     => $maxResources[Resource::MARBLE]
			],
			Resource::SULFUR        => [
				'current' => $curResources[Resource::SULFUR],
				'max'     => $maxResources[Resource::SULFUR]
			],
			Resource::WINE          => [
				'current' => $curResources[Resource::WINE],
				'max'     => $maxResources[Resource::WINE]
			],
			Resource::CRYSTAL_GLASS => [
				'current' => $curResources[Resource::CRYSTAL_GLASS],
				'max'     => $maxResources[Resource::CRYSTAL_GLASS]
			],
		];

		return (new CityEntity())
			->setId($data['id'])
			->setName($data['name'])
			->setCoords(['x' => $data['islandXCoord'], 'y' => $data['islandYCoord']])
			->setPopulation((int)$curResources['population'])
			->setCitizens((int)$curResources['citizens'])
			->setResources($resources)
			->setExtraResource($headerData['producedTradegood'])
			->setWineSpending($headerData['wineSpendings'])
			->setTradegoodProduction($headerData['tradegoodProduction'])
			->setResourceProduction($headerData['resourceProduction'])
			->setIslandId($data['islandId']);
	}
}