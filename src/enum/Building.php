<?php namespace Ikariam\Enum;

/**
 * Enumerace typů budov
 * @package Ikariam\Enum
 */
class Building
{
	/**
	 * Main building in center of the island
	 */
	const MAINHALL = 'mainhall';
}