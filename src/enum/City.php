<?php namespace Ikariam\Enum;

/**
 * City type enumeration
 * @package Ikariam\Enum
 */
class City
{
	const OWN = 'own_city';
	const ALLY = 'ally';
	const OCCUPIED = 'occupied';
	const ENEMY = 'enemy';
}