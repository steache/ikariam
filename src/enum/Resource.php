<?php namespace Ikariam\Enum;

/**
 * Enumeration for resource type
 * @package Ikariam\Enum
 */
class Resource
{
	const WOOD = 0;
	const MARBLE = 2;
	const SULFUR = 4;
	const WINE = 1;
	const CRYSTAL_GLASS = 3;
}