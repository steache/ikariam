# Simple Ikariam api

## Info
0.0.13

### Classes
#### CityManager
##### Methods
 - getCityById(int id)
 - getCityByName(string name)
 - getCityByCoords(int x, int y)
 - getCities()
 - getMyCities()

#### IslandManager
##### Methods
 - getIslandById(int id)
 - getIslandByName(string name)
 - getIslandByCoords(int x, int y)
 - getIslands()

#### City
##### Methods
 - getName()
 - getResources()
 - getResource(int resource)
 - getPopulation()
 - getBuildings()
 - getBuilding(BuildingType building)
 - hasBuilding(BuildingType building)
 - getHappiness() // stejně použije, tak proč né getByType/getMainTown->getHappiness

#### Building
##### Methods
 - getName()
 - getCurrentLevel()
 - getLevel(int level)
 - getLevelResources(int level)
 - getNextLevelResources()

#### Island
##### Methods
 - getName()
 - getId()
 - getCoords()
 - getCities()
 - getExtraResource() // marble/sulfur/wine/crystal glass

#### BuildingType
Enum na typy budov 
 
#### CityType
Enum na typ města, vlastni cizí, okupované apod.