<?php

use Ikariam\Manager\Island;

require('001-login.php');

$islandManager = new Island($requestManager);

$islands = $islandManager->getByArea(0, 5, 0, 5);

foreach($islands as $island)
{
	echo '['.implode(':', $island->getCoords()).'] '.$island->getName() . "\n";
}

try
{
	$island = $islandManager->getByCoords(54, 60);

	echo 'Found ' . $island->getName() . "\n";
}
catch (\Ikariam\IkariamException $e)
{
	echo $e->getMessage() . "\n";
}