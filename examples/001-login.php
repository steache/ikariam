<?php

require('../vendor/autoload.php');

use Ikariam\IkariamException;
use Ikariam\Manager\Request;

$atts = require('../config.php');

$requestManager = new Request($atts);

try
{
	$requestManager->login();

	echo 'Logged in!' . "\n";
}
catch (IkariamException $e)
{
	echo $e->getCode() . ' : ' . $e->getMessage();
}