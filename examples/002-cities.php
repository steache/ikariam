<?php

use Ikariam\Manager\City;

// autoload & login
require('001-login.php');

$cityManager = new City($requestManager);

$cities = $cityManager->getCities();

foreach ($cityManager->getCities() as $city)
{
	echo 'City: ' . $city->getName() . ' coords: ' . implode(':', $city->getCoords()) . "\n";
}
