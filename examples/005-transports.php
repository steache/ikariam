<?php

use Ikariam\Enum\Resource;
use Ikariam\Manager\City as CityManager;
use Ikariam\Manager\Transport;

require('001-login.php');

$cityManager = new CityManager($requestManager);
$transportManager = new Transport($requestManager);

$fromCity = $cityManager->getCityById(100470);
$toCity   = $cityManager->getCityById(101439);

$resources = [
	Resource::SULFUR => 500,
];

try
{
	$transportManager->transport($fromCity, $toCity, $resources);

	echo "Transport sent! \n";
}
catch (\Ikariam\IkariamException $e)
{
	echo "Could not send transport :/ \n";
}