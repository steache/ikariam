Array
(
    [0] => Array
        (
            [0] => updateGlobalData
            [1] => Array
                (
                    [actionRequest] => 3425c70fbed957d481c8faeb36cf0306
                    [time] => 1432564867
                    [activeTab] => 
                    [scrollPos] => 0
                    [wasMinimized] => 0
                    [ingameAd] => <a onclick="ajaxHandlerCall('?view=premiumPayment');return false;"><img class="clickable" src="http://gf1.geo.gfsrv.net/cdn32/f11cdd72f0e22581bfcaa6902dc430.jpg"/></a>
                    [chatNewMessage] => 
                    [animationsActive] => 1
                    [newState] => 0
                    [viewparameters] => {"destinationCityId":"101439"}
                    [storeSliders] => 0
                    [headerData] => Array
                        (
                            [ambrosia] => 29
                            [gold] => 249781.91518637
                            [freeTransporters] => 40
                            [maxTransporters] => 41
                            [relatedCity] => Array
                                (
                                    [owncity] => 1
                                )

                            [currentResources] => Array
                                (
                                    [citizens] => 1182.8499477967
                                    [population] => 1461.8499477967
                                    [resource] => 59838
                                    [2] => 8358
                                    [1] => 28095
                                    [4] => 21517
                                    [3] => 4842
                                )

                            [maxResources] => Array
                                (
                                    [resource] => 226500
                                    [1] => 226500
                                    [2] => 226500
                                    [3] => 226500
                                    [4] => 226500
                                )

                            [branchOfficeResources] => Array
                                (
                                    [resource] => 0
                                    [1] => 0
                                    [2] => 0
                                    [3] => 0
                                    [4] => 0
                                )

                            [resourceProduction] => 0.055555555555556
                            [tradegoodProduction] => 0
                            [producedTradegood] => 4
                            [wineSpendings] => 110
                            [maxActionPoints] => 6
                            [advisors] => Array
                                (
                                    [military] => Array
                                        (
                                            [link] => ?view=militaryAdvisor&oldView=city&destinationCityId=101439
                                            [cssclass] => normal
                                            [premiumlink] => ?view=premiumDetails&oldView=city&destinationCityId=101439
                                        )

                                    [cities] => Array
                                        (
                                            [cssclass] => normal
                                            [link] => ?view=tradeAdvisor&oldView=city&destinationCityId=101439
                                            [premiumlink] => ?view=premiumDetails&oldView=city&destinationCityId=101439
                                        )

                                    [research] => Array
                                        (
                                            [cssclass] => normal
                                            [link] => ?view=researchAdvisor&oldView=city&destinationCityId=101439
                                            [premiumlink] => ?view=premiumDetails&oldView=city&destinationCityId=101439
                                        )

                                    [diplomacy] => Array
                                        (
                                            [cssclass] => normal
                                            [link] => ?view=diplomacyAdvisor&oldView=city&destinationCityId=101439
                                            [premiumlink] => ?view=premiumDetails&oldView=city&destinationCityId=101439
                                        )

                                    [hasPremiumAccount] => 
                                )

                            [cityDropdownMenu] => Array
                                (
                                    [city_100470] => Array
                                        (
                                            [id] => 100470
                                            [name] => Simplicity I
                                            [coords] => [54:60] 
                                            [tradegood] => 4
                                            [relationship] => ownCity
                                        )

                                    [city_101208] => Array
                                        (
                                            [id] => 101208
                                            [name] => Simplicity II
                                            [coords] => [54:59] 
                                            [tradegood] => 1
                                            [relationship] => ownCity
                                        )

                                    [city_101439] => Array
                                        (
                                            [id] => 101439
                                            [name] => Simplicity III
                                            [coords] => [52:59] 
                                            [tradegood] => 2
                                            [relationship] => ownCity
                                        )

                                    [city_103575] => Array
                                        (
                                            [id] => 103575
                                            [name] => Simplicity IV
                                            [coords] => [52:58] 
                                            [tradegood] => 3
                                            [relationship] => ownCity
                                        )

                                    [additionalInfo] => cityCoords
                                    [selectedCity] => city_100470
                                )

                        )

                    [backgroundData] => Array
                        (
                            [name] => Simplicity I
                            [id] => 100470
                            [phase] => 4
                            [ownerId] => 50260
                            [ownerName] => pickwick
                            [islandId] => 244
                            [islandName] => Tejios
                            [islandXCoord] => 54
                            [islandYCoord] => 60
                            [buildingSpeedupActive] => 1
                            [showPirateFortressBackground] => 1
                            [showPirateFortressShip] => 0
                            [lockedPosition] => Array
                                (
                                    [13] => Abyste mohli využít toto stavební místo, musíte mít dokončen výzkum byrokracie
                                )

                            [underConstruction] => -1
                            [endUpgradeTime] => -1
                            [startUpgradeTime] => -1
                            [position] => Array
                                (
                                    [0] => Array
                                        (
                                            [name] => Městská radnice
                                            [level] => 17
                                            [isBusy] => 
                                            [building] => townHall
                                        )

                                    [1] => Array
                                        (
                                            [name] => Obchodní přístav
                                            [level] => 10
                                            [isBusy] => 
                                            [building] => port
                                        )

                                    [2] => Array
                                        (
                                            [name] => Loděnice
                                            [level] => 6
                                            [isBusy] => 
                                            [building] => shipyard
                                        )

                                    [3] => Array
                                        (
                                            [name] => Tesařova dílna
                                            [level] => 15
                                            [isBusy] => 
                                            [building] => carpentering
                                        )

                                    [4] => Array
                                        (
                                            [name] => Úkryt
                                            [level] => 8
                                            [isBusy] => 
                                            [building] => safehouse
                                        )

                                    [5] => Array
                                        (
                                            [name] => Sklad
                                            [level] => 14
                                            [isBusy] => 
                                            [building] => warehouse
                                        )

                                    [6] => Array
                                        (
                                            [name] => Muzeum
                                            [level] => 3
                                            [isBusy] => 
                                            [building] => museum
                                        )

                                    [7] => Array
                                        (
                                            [name] => Dílna
                                            [level] => 8
                                            [isBusy] => 
                                            [building] => workshop
                                        )

                                    [8] => Array
                                        (
                                            [name] => Chrám
                                            [level] => 3
                                            [isBusy] => 
                                            [building] => temple
                                        )

                                    [9] => Array
                                        (
                                            [name] => Kasárna
                                            [level] => 12
                                            [isBusy] => 
                                            [building] => barracks
                                        )

                                    [10] => Array
                                        (
                                            [name] => Palác
                                            [level] => 4
                                            [isBusy] => 
                                            [building] => palace
                                        )

                                    [11] => Array
                                        (
                                            [name] => Tržiště
                                            [level] => 8
                                            [isBusy] => 
                                            [building] => branchOffice
                                        )

                                    [12] => Array
                                        (
                                            [name] => Akademie
                                            [level] => 11
                                            [isBusy] => 
                                            [building] => academy
                                        )

                                    [13] => Array
                                        (
                                            [building] => buildingGround land
                                        )

                                    [14] => Array
                                        (
                                            [name] => Městská zeď
                                            [level] => 9
                                            [isBusy] => 
                                            [building] => wall
                                        )

                                    [15] => Array
                                        (
                                            [building] => buildingGround land
                                        )

                                    [16] => Array
                                        (
                                            [name] => Sklad
                                            [level] => 14
                                            [isBusy] => 
                                            [building] => warehouse
                                        )

                                    [17] => Array
                                        (
                                            [building] => buildingGround sea
                                        )

                                    [18] => Array
                                        (
                                            [name] => Hostinec
                                            [level] => 15
                                            [isBusy] => 
                                            [building] => tavern
                                        )

                                )

                            [beachboys] => visible
                            [spiesInside] => 
                            [cityLeftMenu] => Array
                                (
                                    [visibility] => Array
                                        (
                                            [military] => 1
                                            [espionage] => 0
                                            [resourceShop] => 1
                                            [slot1] => 1
                                            [slot2] => 1
                                            [slot3] => 0
                                            [slot4] => 1
                                        )

                                    [ownCity] => 1
                                )

                            [walkers] => Array
                                (
                                    [add] => Array
                                        (
                                            [0] => Array
                                                (
                                                    [0] => walkerAmbience5728
                                                    [1] => walkers
                                                    [2] => Array
                                                        (
                                                            [0] => Array
                                                                (
                                                                    [0] => 518
                                                                    [1] => 276
                                                                    [2] => none
                                                                    [3] => 310
                                                                    [4] => 1432564860
                                                                    [5] => citizen3
                                                                )

                                                            [1] => Array
                                                                (
                                                                    [0] => 518
                                                                    [1] => 276
                                                                    [2] => SO
                                                                    [3] => 310
                                                                    [4] => 1432564860
                                                                    [5] => citizen3
                                                                )

                                                            [2] => Array
                                                                (
                                                                    [0] => 544
                                                                    [1] => 290
                                                                    [2] => SO
                                                                    [3] => 310
                                                                    [4] => 1432564862
                                                                    [5] => citizen3
                                                                )

                                                            [3] => Array
                                                                (
                                                                    [0] => 552
                                                                    [1] => 323
                                                                    [2] => S
                                                                    [3] => 299
                                                                    [4] => 1432564864
                                                                    [5] => citizen3
                                                                )

                                                            [4] => Array
                                                                (
                                                                    [0] => 590.16666666667
                                                                    [1] => 343.16666666667
                                                                    [2] => S
                                                                    [3] => 299
                                                                    [4] => 1432564867
                                                                    [5] => citizen3
                                                                )

                                                            [5] => Array
                                                                (
                                                                    [0] => 781
                                                                    [1] => 444
                                                                    [2] => SO
                                                                    [3] => 299
                                                                    [4] => 1432564882
                                                                    [5] => citizen3
                                                                )

                                                            [6] => Array
                                                                (
                                                                    [0] => 894
                                                                    [1] => 390
                                                                    [2] => NO
                                                                    [3] => 299
                                                                    [4] => 1432564890
                                                                    [5] => citizen3
                                                                )

                                                            [7] => Array
                                                                (
                                                                    [0] => 931
                                                                    [1] => 406
                                                                    [2] => SO
                                                                    [3] => 299
                                                                    [4] => 1432564893
                                                                    [5] => citizen3
                                                                )

                                                            [8] => Array
                                                                (
                                                                    [0] => 971
                                                                    [1] => 382
                                                                    [2] => NO
                                                                    [3] => 299
                                                                    [4] => 1432564896
                                                                    [5] => citizen3
                                                                )

                                                            [9] => Array
                                                                (
                                                                    [0] => 1016
                                                                    [1] => 418
                                                                    [2] => SO
                                                                    [3] => 310
                                                                    [4] => 1432564900
                                                                    [5] => citizen3
                                                                )

                                                            [10] => Array
                                                                (
                                                                    [0] => 971
                                                                    [1] => 382
                                                                    [2] => NW
                                                                    [3] => 310
                                                                    [4] => 1432564903
                                                                    [5] => citizen3
                                                                )

                                                            [11] => Array
                                                                (
                                                                    [0] => 931
                                                                    [1] => 406
                                                                    [2] => SW
                                                                    [3] => 299
                                                                    [4] => 1432564906
                                                                    [5] => citizen3
                                                                )

                                                            [12] => Array
                                                                (
                                                                    [0] => 894
                                                                    [1] => 390
                                                                    [2] => NW
                                                                    [3] => 299
                                                                    [4] => 1432564909
                                                                    [5] => citizen3
                                                                )

                                                            [13] => Array
                                                                (
                                                                    [0] => 781
                                                                    [1] => 444
                                                                    [2] => SW
                                                                    [3] => 299
                                                                    [4] => 1432564917
                                                                    [5] => citizen3
                                                                )

                                                            [14] => Array
                                                                (
                                                                    [0] => 552
                                                                    [1] => 323
                                                                    [2] => NW
                                                                    [3] => 299
                                                                    [4] => 1432564935
                                                                    [5] => citizen3
                                                                )

                                                            [15] => Array
                                                                (
                                                                    [0] => 544
                                                                    [1] => 290
                                                                    [2] => N
                                                                    [3] => 299
                                                                    [4] => 1432564937
                                                                    [5] => citizen3
                                                                )

                                                            [16] => Array
                                                                (
                                                                    [0] => 518
                                                                    [1] => 276
                                                                    [2] => NW
                                                                    [3] => 310
                                                                    [4] => 1432564939
                                                                    [5] => citizen3
                                                                )

                                                        )

                                                    [3] => 1432564867
                                                    [4] => Krásný den na procházku!
                                                    [5] => 1432564939
                                                    [6] => 0
                                                )

                                            [1] => Array
                                                (
                                                    [0] => walkerAmbience5729
                                                    [1] => walkers
                                                    [2] => Array
                                                        (
                                                            [0] => Array
                                                                (
                                                                    [0] => 1170
                                                                    [1] => 377
                                                                    [2] => none
                                                                    [3] => 310
                                                                    [4] => 1432564860
                                                                    [5] => soldierOwn
                                                                )

                                                            [1] => Array
                                                                (
                                                                    [0] => 1170
                                                                    [1] => 377
                                                                    [2] => NW
                                                                    [3] => 310
                                                                    [4] => 1432564860
                                                                    [5] => soldierOwn
                                                                )

                                                            [2] => Array
                                                                (
                                                                    [0] => 1124
                                                                    [1] => 337
                                                                    [2] => NW
                                                                    [3] => 310
                                                                    [4] => 1432564864
                                                                    [5] => soldierOwn
                                                                )

                                                            [3] => Array
                                                                (
                                                                    [0] => 1096
                                                                    [1] => 350
                                                                    [2] => SW
                                                                    [3] => 310
                                                                    [4] => 1432564866
                                                                    [5] => soldierOwn
                                                                )

                                                            [4] => Array
                                                                (
                                                                    [0] => 1083
                                                                    [1] => 346
                                                                    [2] => SW
                                                                    [3] => 310
                                                                    [4] => 1432564867
                                                                    [5] => soldierOwn
                                                                )

                                                            [5] => Array
                                                                (
                                                                    [0] => 1057
                                                                    [1] => 338
                                                                    [2] => W
                                                                    [3] => 308
                                                                    [4] => 1432564869
                                                                    [5] => soldierOwn
                                                                )

                                                            [6] => Array
                                                                (
                                                                    [0] => 969
                                                                    [1] => 380
                                                                    [2] => SW
                                                                    [3] => 308
                                                                    [4] => 1432564876
                                                                    [5] => soldierOwn
                                                                )

                                                            [7] => Array
                                                                (
                                                                    [0] => 929
                                                                    [1] => 404
                                                                    [2] => SW
                                                                    [3] => 299
                                                                    [4] => 1432564879
                                                                    [5] => soldierOwn
                                                                )

                                                            [8] => Array
                                                                (
                                                                    [0] => 969
                                                                    [1] => 440
                                                                    [2] => SO
                                                                    [3] => 299
                                                                    [4] => 1432564882
                                                                    [5] => soldierOwn
                                                                )

                                                            [9] => Array
                                                                (
                                                                    [0] => 859
                                                                    [1] => 503
                                                                    [2] => SW
                                                                    [3] => 310
                                                                    [4] => 1432564891
                                                                    [5] => soldierOwn
                                                                )

                                                            [10] => Array
                                                                (
                                                                    [0] => 905
                                                                    [1] => 539
                                                                    [2] => SO
                                                                    [3] => 299
                                                                    [4] => 1432564895
                                                                    [5] => soldierOwn
                                                                )

                                                            [11] => Array
                                                                (
                                                                    [0] => 931
                                                                    [1] => 580
                                                                    [2] => SO
                                                                    [3] => 299
                                                                    [4] => 1432564898
                                                                    [5] => soldierOwn
                                                                )

                                                            [12] => Array
                                                                (
                                                                    [0] => 865
                                                                    [1] => 615
                                                                    [2] => SW
                                                                    [3] => 310
                                                                    [4] => 1432564903
                                                                    [5] => soldierOwn
                                                                )

                                                            [13] => Array
                                                                (
                                                                    [0] => 834
                                                                    [1] => 617
                                                                    [2] => W
                                                                    [3] => 310
                                                                    [4] => 1432564905
                                                                    [5] => soldierOwn
                                                                )

                                                            [14] => Array
                                                                (
                                                                    [0] => 815
                                                                    [1] => 597
                                                                    [2] => NW
                                                                    [3] => 310
                                                                    [4] => 1432564907
                                                                    [5] => soldierOwn
                                                                )

                                                            [15] => Array
                                                                (
                                                                    [0] => 834
                                                                    [1] => 617
                                                                    [2] => SO
                                                                    [3] => 310
                                                                    [4] => 1432564909
                                                                    [5] => soldierOwn
                                                                )

                                                            [16] => Array
                                                                (
                                                                    [0] => 865
                                                                    [1] => 615
                                                                    [2] => O
                                                                    [3] => 310
                                                                    [4] => 1432564911
                                                                    [5] => soldierOwn
                                                                )

                                                            [17] => Array
                                                                (
                                                                    [0] => 931
                                                                    [1] => 580
                                                                    [2] => NO
                                                                    [3] => 310
                                                                    [4] => 1432564916
                                                                    [5] => soldierOwn
                                                                )

                                                            [18] => Array
                                                                (
                                                                    [0] => 905
                                                                    [1] => 539
                                                                    [2] => NW
                                                                    [3] => 299
                                                                    [4] => 1432564919
                                                                    [5] => soldierOwn
                                                                )

                                                            [19] => Array
                                                                (
                                                                    [0] => 859
                                                                    [1] => 503
                                                                    [2] => NW
                                                                    [3] => 299
                                                                    [4] => 1432564923
                                                                    [5] => soldierOwn
                                                                )

                                                            [20] => Array
                                                                (
                                                                    [0] => 969
                                                                    [1] => 440
                                                                    [2] => NO
                                                                    [3] => 310
                                                                    [4] => 1432564932
                                                                    [5] => soldierOwn
                                                                )

                                                            [21] => Array
                                                                (
                                                                    [0] => 929
                                                                    [1] => 404
                                                                    [2] => NW
                                                                    [3] => 299
                                                                    [4] => 1432564935
                                                                    [5] => soldierOwn
                                                                )

                                                            [22] => Array
                                                                (
                                                                    [0] => 969
                                                                    [1] => 380
                                                                    [2] => NO
                                                                    [3] => 299
                                                                    [4] => 1432564938
                                                                    [5] => soldierOwn
                                                                )

                                                            [23] => Array
                                                                (
                                                                    [0] => 1057
                                                                    [1] => 338
                                                                    [2] => NO
                                                                    [3] => 308
                                                                    [4] => 1432564945
                                                                    [5] => soldierOwn
                                                                )

                                                            [24] => Array
                                                                (
                                                                    [0] => 1096
                                                                    [1] => 350
                                                                    [2] => O
                                                                    [3] => 308
                                                                    [4] => 1432564948
                                                                    [5] => soldierOwn
                                                                )

                                                            [25] => Array
                                                                (
                                                                    [0] => 1124
                                                                    [1] => 337
                                                                    [2] => NO
                                                                    [3] => 310
                                                                    [4] => 1432564950
                                                                    [5] => soldierOwn
                                                                )

                                                            [26] => Array
                                                                (
                                                                    [0] => 1170
                                                                    [1] => 377
                                                                    [2] => SO
                                                                    [3] => 310
                                                                    [4] => 1432564954
                                                                    [5] => soldierOwn
                                                                )

                                                        )

                                                    [3] => 1432564867
                                                    [4] => Miluji běhání!
                                                    [5] => 1432564954
                                                    [6] => 0
                                                )

                                            [2] => Array
                                                (
                                                    [0] => walkerAmbience5730
                                                    [1] => walkers
                                                    [2] => Array
                                                        (
                                                            [0] => Array
                                                                (
                                                                    [0] => 1178
                                                                    [1] => 534
                                                                    [2] => none
                                                                    [3] => 310
                                                                    [4] => 1432564860
                                                                    [5] => citizen2
                                                                )

                                                            [1] => Array
                                                                (
                                                                    [0] => 1178
                                                                    [1] => 534
                                                                    [2] => SW
                                                                    [3] => 310
                                                                    [4] => 1432564860
                                                                    [5] => citizen2
                                                                )

                                                            [2] => Array
                                                                (
                                                                    [0] => 1169
                                                                    [1] => 541
                                                                    [2] => SW
                                                                    [3] => 310
                                                                    [4] => 1432564861
                                                                    [5] => citizen2
                                                                )

                                                            [3] => Array
                                                                (
                                                                    [0] => 1146
                                                                    [1] => 531
                                                                    [2] => NW
                                                                    [3] => 310
                                                                    [4] => 1432564862
                                                                    [5] => citizen2
                                                                )

                                                            [4] => Array
                                                                (
                                                                    [0] => 1121
                                                                    [1] => 514
                                                                    [2] => NW
                                                                    [3] => 310
                                                                    [4] => 1432564864
                                                                    [5] => citizen2
                                                                )

                                                            [5] => Array
                                                                (
                                                                    [0] => 1083.5
                                                                    [1] => 496
                                                                    [2] => NW
                                                                    [3] => 310
                                                                    [4] => 1432564867
                                                                    [5] => citizen2
                                                                )

                                                            [6] => Array
                                                                (
                                                                    [0] => 971
                                                                    [1] => 442
                                                                    [2] => NW
                                                                    [3] => 299
                                                                    [4] => 1432564876
                                                                    [5] => citizen2
                                                                )

                                                            [7] => Array
                                                                (
                                                                    [0] => 861
                                                                    [1] => 505
                                                                    [2] => SW
                                                                    [3] => 310
                                                                    [4] => 1432564884
                                                                    [5] => citizen2
                                                                )

                                                            [8] => Array
                                                                (
                                                                    [0] => 823
                                                                    [1] => 522
                                                                    [2] => SW
                                                                    [3] => 299
                                                                    [4] => 1432564887
                                                                    [5] => citizen2
                                                                )

                                                            [9] => Array
                                                                (
                                                                    [0] => 761
                                                                    [1] => 555
                                                                    [2] => SW
                                                                    [3] => 299
                                                                    [4] => 1432564892
                                                                    [5] => citizen2
                                                                )

                                                            [10] => Array
                                                                (
                                                                    [0] => 704
                                                                    [1] => 582
                                                                    [2] => SW
                                                                    [3] => 310
                                                                    [4] => 1432564896
                                                                    [5] => citizen2
                                                                )

                                                            [11] => Array
                                                                (
                                                                    [0] => 631
                                                                    [1] => 618
                                                                    [2] => SW
                                                                    [3] => 310
                                                                    [4] => 1432564901
                                                                    [5] => citizen2
                                                                )

                                                            [12] => Array
                                                                (
                                                                    [0] => 632
                                                                    [1] => 678
                                                                    [2] => S
                                                                    [3] => 310
                                                                    [4] => 1432564905
                                                                    [5] => citizen2
                                                                )

                                                            [13] => Array
                                                                (
                                                                    [0] => 686
                                                                    [1] => 707
                                                                    [2] => SO
                                                                    [3] => 299
                                                                    [4] => 1432564910
                                                                    [5] => citizen2
                                                                )

                                                            [14] => Array
                                                                (
                                                                    [0] => 632
                                                                    [1] => 678
                                                                    [2] => NW
                                                                    [3] => 299
                                                                    [4] => 1432564914
                                                                    [5] => citizen2
                                                                )

                                                            [15] => Array
                                                                (
                                                                    [0] => 631
                                                                    [1] => 618
                                                                    [2] => N
                                                                    [3] => 310
                                                                    [4] => 1432564918
                                                                    [5] => citizen2
                                                                )

                                                            [16] => Array
                                                                (
                                                                    [0] => 704
                                                                    [1] => 582
                                                                    [2] => NO
                                                                    [3] => 310
                                                                    [4] => 1432564923
                                                                    [5] => citizen2
                                                                )

                                                            [17] => Array
                                                                (
                                                                    [0] => 761
                                                                    [1] => 555
                                                                    [2] => NO
                                                                    [3] => 310
                                                                    [4] => 1432564927
                                                                    [5] => citizen2
                                                                )

                                                            [18] => Array
                                                                (
                                                                    [0] => 823
                                                                    [1] => 522
                                                                    [2] => NO
                                                                    [3] => 299
                                                                    [4] => 1432564932
                                                                    [5] => citizen2
                                                                )

                                                            [19] => Array
                                                                (
                                                                    [0] => 861
                                                                    [1] => 505
                                                                    [2] => NO
                                                                    [3] => 299
                                                                    [4] => 1432564935
                                                                    [5] => citizen2
                                                                )

                                                            [20] => Array
                                                                (
                                                                    [0] => 971
                                                                    [1] => 442
                                                                    [2] => NO
                                                                    [3] => 310
                                                                    [4] => 1432564943
                                                                    [5] => citizen2
                                                                )

                                                            [21] => Array
                                                                (
                                                                    [0] => 1121
                                                                    [1] => 514
                                                                    [2] => SO
                                                                    [3] => 299
                                                                    [4] => 1432564955
                                                                    [5] => citizen2
                                                                )

                                                            [22] => Array
                                                                (
                                                                    [0] => 1146
                                                                    [1] => 531
                                                                    [2] => SO
                                                                    [3] => 310
                                                                    [4] => 1432564957
                                                                    [5] => citizen2
                                                                )

                                                            [23] => Array
                                                                (
                                                                    [0] => 1169
                                                                    [1] => 541
                                                                    [2] => SO
                                                                    [3] => 310
                                                                    [4] => 1432564958
                                                                    [5] => citizen2
                                                                )

                                                            [24] => Array
                                                                (
                                                                    [0] => 1178
                                                                    [1] => 534
                                                                    [2] => NO
                                                                    [3] => 310
                                                                    [4] => 1432564959
                                                                    [5] => citizen2
                                                                )

                                                        )

                                                    [3] => 1432564867
                                                    [4] => Pojďme si poslechnout nejnovější drby!
                                                    [5] => 1432564959
                                                    [6] => 0
                                                )

                                            [3] => Array
                                                (
                                                    [0] => walkerAmbience5731
                                                    [1] => walkers
                                                    [2] => Array
                                                        (
                                                            [0] => Array
                                                                (
                                                                    [0] => 874
                                                                    [1] => 321
                                                                    [2] => none
                                                                    [3] => 310
                                                                    [4] => 1432564867
                                                                    [5] => citizen2
                                                                )

                                                            [1] => Array
                                                                (
                                                                    [0] => 874
                                                                    [1] => 321
                                                                    [2] => SO
                                                                    [3] => 310
                                                                    [4] => 1432564867
                                                                    [5] => citizen2
                                                                )

                                                            [2] => Array
                                                                (
                                                                    [0] => 874
                                                                    [1] => 321
                                                                    [2] => SO
                                                                    [3] => 310
                                                                    [4] => 1432564867
                                                                    [5] => citizen2
                                                                )

                                                            [3] => Array
                                                                (
                                                                    [0] => 892
                                                                    [1] => 340
                                                                    [2] => SO
                                                                    [3] => 310
                                                                    [4] => 1432564869
                                                                    [5] => citizen2
                                                                )

                                                            [4] => Array
                                                                (
                                                                    [0] => 971
                                                                    [1] => 382
                                                                    [2] => SO
                                                                    [3] => 310
                                                                    [4] => 1432564875
                                                                    [5] => citizen2
                                                                )

                                                            [5] => Array
                                                                (
                                                                    [0] => 931
                                                                    [1] => 406
                                                                    [2] => SW
                                                                    [3] => 299
                                                                    [4] => 1432564878
                                                                    [5] => citizen2
                                                                )

                                                            [6] => Array
                                                                (
                                                                    [0] => 971
                                                                    [1] => 442
                                                                    [2] => SO
                                                                    [3] => 299
                                                                    [4] => 1432564881
                                                                    [5] => citizen2
                                                                )

                                                            [7] => Array
                                                                (
                                                                    [0] => 861
                                                                    [1] => 505
                                                                    [2] => SW
                                                                    [3] => 310
                                                                    [4] => 1432564890
                                                                    [5] => citizen2
                                                                )

                                                            [8] => Array
                                                                (
                                                                    [0] => 823
                                                                    [1] => 522
                                                                    [2] => SW
                                                                    [3] => 299
                                                                    [4] => 1432564893
                                                                    [5] => citizen2
                                                                )

                                                            [9] => Array
                                                                (
                                                                    [0] => 761
                                                                    [1] => 555
                                                                    [2] => SW
                                                                    [3] => 299
                                                                    [4] => 1432564897
                                                                    [5] => citizen2
                                                                )

                                                            [10] => Array
                                                                (
                                                                    [0] => 704
                                                                    [1] => 582
                                                                    [2] => SW
                                                                    [3] => 310
                                                                    [4] => 1432564902
                                                                    [5] => citizen2
                                                                )

                                                            [11] => Array
                                                                (
                                                                    [0] => 631
                                                                    [1] => 618
                                                                    [2] => SW
                                                                    [3] => 310
                                                                    [4] => 1432564907
                                                                    [5] => citizen2
                                                                )

                                                            [12] => Array
                                                                (
                                                                    [0] => 593
                                                                    [1] => 590
                                                                    [2] => NW
                                                                    [3] => 310
                                                                    [4] => 1432564910
                                                                    [5] => citizen2
                                                                )

                                                            [13] => Array
                                                                (
                                                                    [0] => 615
                                                                    [1] => 579
                                                                    [2] => NO
                                                                    [3] => 310
                                                                    [4] => 1432564912
                                                                    [5] => citizen2
                                                                )

                                                            [14] => Array
                                                                (
                                                                    [0] => 593
                                                                    [1] => 590
                                                                    [2] => SW
                                                                    [3] => 310
                                                                    [4] => 1432564914
                                                                    [5] => citizen2
                                                                )

                                                            [15] => Array
                                                                (
                                                                    [0] => 631
                                                                    [1] => 618
                                                                    [2] => SO
                                                                    [3] => 310
                                                                    [4] => 1432564917
                                                                    [5] => citizen2
                                                                )

                                                            [16] => Array
                                                                (
                                                                    [0] => 704
                                                                    [1] => 582
                                                                    [2] => NO
                                                                    [3] => 310
                                                                    [4] => 1432564922
                                                                    [5] => citizen2
                                                                )

                                                            [17] => Array
                                                                (
                                                                    [0] => 761
                                                                    [1] => 555
                                                                    [2] => NO
                                                                    [3] => 310
                                                                    [4] => 1432564927
                                                                    [5] => citizen2
                                                                )

                                                            [18] => Array
                                                                (
                                                                    [0] => 823
                                                                    [1] => 522
                                                                    [2] => NO
                                                                    [3] => 299
                                                                    [4] => 1432564931
                                                                    [5] => citizen2
                                                                )

                                                            [19] => Array
                                                                (
                                                                    [0] => 861
                                                                    [1] => 505
                                                                    [2] => NO
                                                                    [3] => 299
                                                                    [4] => 1432564934
                                                                    [5] => citizen2
                                                                )

                                                            [20] => Array
                                                                (
                                                                    [0] => 971
                                                                    [1] => 442
                                                                    [2] => NO
                                                                    [3] => 310
                                                                    [4] => 1432564943
                                                                    [5] => citizen2
                                                                )

                                                            [21] => Array
                                                                (
                                                                    [0] => 931
                                                                    [1] => 406
                                                                    [2] => NW
                                                                    [3] => 299
                                                                    [4] => 1432564946
                                                                    [5] => citizen2
                                                                )

                                                            [22] => Array
                                                                (
                                                                    [0] => 971
                                                                    [1] => 382
                                                                    [2] => NO
                                                                    [3] => 299
                                                                    [4] => 1432564949
                                                                    [5] => citizen2
                                                                )

                                                            [23] => Array
                                                                (
                                                                    [0] => 892
                                                                    [1] => 340
                                                                    [2] => NW
                                                                    [3] => 310
                                                                    [4] => 1432564955
                                                                    [5] => citizen2
                                                                )

                                                            [24] => Array
                                                                (
                                                                    [0] => 874
                                                                    [1] => 321
                                                                    [2] => NW
                                                                    [3] => 310
                                                                    [4] => 1432564957
                                                                    [5] => citizen2
                                                                )

                                                        )

                                                    [3] => 1432564867
                                                    [4] => Ptáci odletěli do hnízd… vypadá to, že by mohlo pršet!
                                                    [5] => 1432564957
                                                    [6] => 0
                                                )

                                            [4] => Array
                                                (
                                                    [0] => walkerAmbience5732
                                                    [1] => walkers
                                                    [2] => Array
                                                        (
                                                            [0] => Array
                                                                (
                                                                    [0] => 1172
                                                                    [1] => 379
                                                                    [2] => none
                                                                    [3] => 310
                                                                    [4] => 1432564870
                                                                    [5] => citizen4
                                                                )

                                                            [1] => Array
                                                                (
                                                                    [0] => 1172
                                                                    [1] => 379
                                                                    [2] => NW
                                                                    [3] => 310
                                                                    [4] => 1432564870
                                                                    [5] => citizen4
                                                                )

                                                            [2] => Array
                                                                (
                                                                    [0] => 1126
                                                                    [1] => 339
                                                                    [2] => NW
                                                                    [3] => 310
                                                                    [4] => 1432564874
                                                                    [5] => citizen4
                                                                )

                                                            [3] => Array
                                                                (
                                                                    [0] => 1098
                                                                    [1] => 352
                                                                    [2] => SW
                                                                    [3] => 310
                                                                    [4] => 1432564876
                                                                    [5] => citizen4
                                                                )

                                                            [4] => Array
                                                                (
                                                                    [0] => 1059
                                                                    [1] => 340
                                                                    [2] => W
                                                                    [3] => 308
                                                                    [4] => 1432564879
                                                                    [5] => citizen4
                                                                )

                                                            [5] => Array
                                                                (
                                                                    [0] => 971
                                                                    [1] => 382
                                                                    [2] => SW
                                                                    [3] => 308
                                                                    [4] => 1432564886
                                                                    [5] => citizen4
                                                                )

                                                            [6] => Array
                                                                (
                                                                    [0] => 1016
                                                                    [1] => 418
                                                                    [2] => SO
                                                                    [3] => 310
                                                                    [4] => 1432564890
                                                                    [5] => citizen4
                                                                )

                                                            [7] => Array
                                                                (
                                                                    [0] => 971
                                                                    [1] => 382
                                                                    [2] => NW
                                                                    [3] => 310
                                                                    [4] => 1432564893
                                                                    [5] => citizen4
                                                                )

                                                            [8] => Array
                                                                (
                                                                    [0] => 1059
                                                                    [1] => 340
                                                                    [2] => NO
                                                                    [3] => 308
                                                                    [4] => 1432564900
                                                                    [5] => citizen4
                                                                )

                                                            [9] => Array
                                                                (
                                                                    [0] => 1098
                                                                    [1] => 352
                                                                    [2] => O
                                                                    [3] => 308
                                                                    [4] => 1432564903
                                                                    [5] => citizen4
                                                                )

                                                            [10] => Array
                                                                (
                                                                    [0] => 1126
                                                                    [1] => 339
                                                                    [2] => NO
                                                                    [3] => 310
                                                                    [4] => 1432564905
                                                                    [5] => citizen4
                                                                )

                                                            [11] => Array
                                                                (
                                                                    [0] => 1172
                                                                    [1] => 379
                                                                    [2] => SO
                                                                    [3] => 310
                                                                    [4] => 1432564909
                                                                    [5] => citizen4
                                                                )

                                                        )

                                                    [3] => 1432564867
                                                    [4] => V tomto městě je život skvělý!
                                                    [5] => 1432564909
                                                    [6] => 0
                                                )

                                            [5] => Array
                                                (
                                                    [0] => walkerAmbience5733
                                                    [1] => walkers
                                                    [2] => Array
                                                        (
                                                            [0] => Array
                                                                (
                                                                    [0] => 454
                                                                    [1] => 541
                                                                    [2] => none
                                                                    [3] => 310
                                                                    [4] => 1432564861
                                                                    [5] => citizen3
                                                                )

                                                            [1] => Array
                                                                (
                                                                    [0] => 454
                                                                    [1] => 541
                                                                    [2] => SO
                                                                    [3] => 310
                                                                    [4] => 1432564861
                                                                    [5] => citizen3
                                                                )

                                                            [2] => Array
                                                                (
                                                                    [0] => 490
                                                                    [1] => 560
                                                                    [2] => SO
                                                                    [3] => 310
                                                                    [4] => 1432564864
                                                                    [5] => citizen3
                                                                )

                                                            [3] => Array
                                                                (
                                                                    [0] => 534
                                                                    [1] => 545
                                                                    [2] => O
                                                                    [3] => 310
                                                                    [4] => 1432564867
                                                                    [5] => citizen3
                                                                )

                                                            [4] => Array
                                                                (
                                                                    [0] => 534
                                                                    [1] => 545
                                                                    [2] => O
                                                                    [3] => 310
                                                                    [4] => 1432564867
                                                                    [5] => citizen3
                                                                )

                                                            [5] => Array
                                                                (
                                                                    [0] => 593
                                                                    [1] => 590
                                                                    [2] => SO
                                                                    [3] => 310
                                                                    [4] => 1432564872
                                                                    [5] => citizen3
                                                                )

                                                            [6] => Array
                                                                (
                                                                    [0] => 631
                                                                    [1] => 618
                                                                    [2] => SO
                                                                    [3] => 310
                                                                    [4] => 1432564875
                                                                    [5] => citizen3
                                                                )

                                                            [7] => Array
                                                                (
                                                                    [0] => 704
                                                                    [1] => 582
                                                                    [2] => NO
                                                                    [3] => 310
                                                                    [4] => 1432564880
                                                                    [5] => citizen3
                                                                )

                                                            [8] => Array
                                                                (
                                                                    [0] => 761
                                                                    [1] => 555
                                                                    [2] => NO
                                                                    [3] => 310
                                                                    [4] => 1432564885
                                                                    [5] => citizen3
                                                                )

                                                            [9] => Array
                                                                (
                                                                    [0] => 823
                                                                    [1] => 522
                                                                    [2] => NO
                                                                    [3] => 299
                                                                    [4] => 1432564889
                                                                    [5] => citizen3
                                                                )

                                                            [10] => Array
                                                                (
                                                                    [0] => 861
                                                                    [1] => 505
                                                                    [2] => NO
                                                                    [3] => 299
                                                                    [4] => 1432564892
                                                                    [5] => citizen3
                                                                )

                                                            [11] => Array
                                                                (
                                                                    [0] => 971
                                                                    [1] => 442
                                                                    [2] => NO
                                                                    [3] => 310
                                                                    [4] => 1432564900
                                                                    [5] => citizen3
                                                                )

                                                            [12] => Array
                                                                (
                                                                    [0] => 1121
                                                                    [1] => 514
                                                                    [2] => SO
                                                                    [3] => 299
                                                                    [4] => 1432564912
                                                                    [5] => citizen3
                                                                )

                                                            [13] => Array
                                                                (
                                                                    [0] => 1146
                                                                    [1] => 531
                                                                    [2] => SO
                                                                    [3] => 310
                                                                    [4] => 1432564914
                                                                    [5] => citizen3
                                                                )

                                                            [14] => Array
                                                                (
                                                                    [0] => 1169
                                                                    [1] => 541
                                                                    [2] => SO
                                                                    [3] => 310
                                                                    [4] => 1432564915
                                                                    [5] => citizen3
                                                                )

                                                            [15] => Array
                                                                (
                                                                    [0] => 1178
                                                                    [1] => 534
                                                                    [2] => NO
                                                                    [3] => 310
                                                                    [4] => 1432564916
                                                                    [5] => citizen3
                                                                )

                                                            [16] => Array
                                                                (
                                                                    [0] => 1169
                                                                    [1] => 541
                                                                    [2] => SW
                                                                    [3] => 310
                                                                    [4] => 1432564917
                                                                    [5] => citizen3
                                                                )

                                                            [17] => Array
                                                                (
                                                                    [0] => 1146
                                                                    [1] => 531
                                                                    [2] => NW
                                                                    [3] => 310
                                                                    [4] => 1432564918
                                                                    [5] => citizen3
                                                                )

                                                            [18] => Array
                                                                (
                                                                    [0] => 1121
                                                                    [1] => 514
                                                                    [2] => NW
                                                                    [3] => 310
                                                                    [4] => 1432564920
                                                                    [5] => citizen3
                                                                )

                                                            [19] => Array
                                                                (
                                                                    [0] => 971
                                                                    [1] => 442
                                                                    [2] => NW
                                                                    [3] => 299
                                                                    [4] => 1432564932
                                                                    [5] => citizen3
                                                                )

                                                            [20] => Array
                                                                (
                                                                    [0] => 861
                                                                    [1] => 505
                                                                    [2] => SW
                                                                    [3] => 310
                                                                    [4] => 1432564940
                                                                    [5] => citizen3
                                                                )

                                                            [21] => Array
                                                                (
                                                                    [0] => 823
                                                                    [1] => 522
                                                                    [2] => SW
                                                                    [3] => 299
                                                                    [4] => 1432564943
                                                                    [5] => citizen3
                                                                )

                                                            [22] => Array
                                                                (
                                                                    [0] => 761
                                                                    [1] => 555
                                                                    [2] => SW
                                                                    [3] => 299
                                                                    [4] => 1432564947
                                                                    [5] => citizen3
                                                                )

                                                            [23] => Array
                                                                (
                                                                    [0] => 704
                                                                    [1] => 582
                                                                    [2] => SW
                                                                    [3] => 310
                                                                    [4] => 1432564952
                                                                    [5] => citizen3
                                                                )

                                                            [24] => Array
                                                                (
                                                                    [0] => 631
                                                                    [1] => 618
                                                                    [2] => SW
                                                                    [3] => 310
                                                                    [4] => 1432564957
                                                                    [5] => citizen3
                                                                )

                                                            [25] => Array
                                                                (
                                                                    [0] => 593
                                                                    [1] => 590
                                                                    [2] => NW
                                                                    [3] => 310
                                                                    [4] => 1432564960
                                                                    [5] => citizen3
                                                                )

                                                            [26] => Array
                                                                (
                                                                    [0] => 534
                                                                    [1] => 545
                                                                    [2] => NW
                                                                    [3] => 310
                                                                    [4] => 1432564965
                                                                    [5] => citizen3
                                                                )

                                                            [27] => Array
                                                                (
                                                                    [0] => 490
                                                                    [1] => 560
                                                                    [2] => W
                                                                    [3] => 310
                                                                    [4] => 1432564968
                                                                    [5] => citizen3
                                                                )

                                                            [28] => Array
                                                                (
                                                                    [0] => 454
                                                                    [1] => 541
                                                                    [2] => NW
                                                                    [3] => 310
                                                                    [4] => 1432564971
                                                                    [5] => citizen3
                                                                )

                                                        )

                                                    [3] => 1432564867
                                                    [4] => Musím zase brzy navštívit své příbuzné!
                                                    [5] => 1432564971
                                                    [6] => 0
                                                )

                                            [6] => Array
                                                (
                                                    [0] => walkerAmbience5734
                                                    [1] => walkers
                                                    [2] => Array
                                                        (
                                                            [0] => Array
                                                                (
                                                                    [0] => 1176
                                                                    [1] => 532
                                                                    [2] => none
                                                                    [3] => 310
                                                                    [4] => 1432564868
                                                                    [5] => soldierOwn
                                                                )

                                                            [1] => Array
                                                                (
                                                                    [0] => 1176
                                                                    [1] => 532
                                                                    [2] => SW
                                                                    [3] => 310
                                                                    [4] => 1432564868
                                                                    [5] => soldierOwn
                                                                )

                                                            [2] => Array
                                                                (
                                                                    [0] => 1167
                                                                    [1] => 539
                                                                    [2] => SW
                                                                    [3] => 310
                                                                    [4] => 1432564869
                                                                    [5] => soldierOwn
                                                                )

                                                            [3] => Array
                                                                (
                                                                    [0] => 1144
                                                                    [1] => 529
                                                                    [2] => NW
                                                                    [3] => 310
                                                                    [4] => 1432564870
                                                                    [5] => soldierOwn
                                                                )

                                                            [4] => Array
                                                                (
                                                                    [0] => 1119
                                                                    [1] => 512
                                                                    [2] => NW
                                                                    [3] => 310
                                                                    [4] => 1432564873
                                                                    [5] => soldierOwn
                                                                )

                                                            [5] => Array
                                                                (
                                                                    [0] => 969
                                                                    [1] => 440
                                                                    [2] => NW
                                                                    [3] => 299
                                                                    [4] => 1432564884
                                                                    [5] => soldierOwn
                                                                )

                                                            [6] => Array
                                                                (
                                                                    [0] => 929
                                                                    [1] => 404
                                                                    [2] => NW
                                                                    [3] => 299
                                                                    [4] => 1432564887
                                                                    [5] => soldierOwn
                                                                )

                                                            [7] => Array
                                                                (
                                                                    [0] => 969
                                                                    [1] => 380
                                                                    [2] => NO
                                                                    [3] => 299
                                                                    [4] => 1432564891
                                                                    [5] => soldierOwn
                                                                )

                                                            [8] => Array
                                                                (
                                                                    [0] => 1014
                                                                    [1] => 416
                                                                    [2] => SO
                                                                    [3] => 310
                                                                    [4] => 1432564895
                                                                    [5] => soldierOwn
                                                                )

                                                            [9] => Array
                                                                (
                                                                    [0] => 969
                                                                    [1] => 380
                                                                    [2] => NW
                                                                    [3] => 310
                                                                    [4] => 1432564898
                                                                    [5] => soldierOwn
                                                                )

                                                            [10] => Array
                                                                (
                                                                    [0] => 929
                                                                    [1] => 404
                                                                    [2] => SW
                                                                    [3] => 299
                                                                    [4] => 1432564902
                                                                    [5] => soldierOwn
                                                                )

                                                            [11] => Array
                                                                (
                                                                    [0] => 969
                                                                    [1] => 440
                                                                    [2] => SO
                                                                    [3] => 299
                                                                    [4] => 1432564905
                                                                    [5] => soldierOwn
                                                                )

                                                            [12] => Array
                                                                (
                                                                    [0] => 1119
                                                                    [1] => 512
                                                                    [2] => SO
                                                                    [3] => 299
                                                                    [4] => 1432564916
                                                                    [5] => soldierOwn
                                                                )

                                                            [13] => Array
                                                                (
                                                                    [0] => 1144
                                                                    [1] => 529
                                                                    [2] => SO
                                                                    [3] => 310
                                                                    [4] => 1432564919
                                                                    [5] => soldierOwn
                                                                )

                                                            [14] => Array
                                                                (
                                                                    [0] => 1167
                                                                    [1] => 539
                                                                    [2] => SO
                                                                    [3] => 310
                                                                    [4] => 1432564920
                                                                    [5] => soldierOwn
                                                                )

                                                            [15] => Array
                                                                (
                                                                    [0] => 1176
                                                                    [1] => 532
                                                                    [2] => NO
                                                                    [3] => 310
                                                                    [4] => 1432564921
                                                                    [5] => soldierOwn
                                                                )

                                                        )

                                                    [3] => 1432564867
                                                    [4] => Naše ekonomika prosperuje!
                                                    [5] => 1432564921
                                                    [6] => 0
                                                )

                                            [7] => Array
                                                (
                                                    [0] => walkerProduction_resource_100470
                                                    [1] => walkers
                                                    [2] => Array
                                                        (
                                                            [0] => Array
                                                                (
                                                                    [0] => 112
                                                                    [1] => -49
                                                                    [2] => none
                                                                    [3] => 299
                                                                    [4] => 1432564660
                                                                    [5] => resourceWalker
                                                                )

                                                            [1] => Array
                                                                (
                                                                    [0] => 112
                                                                    [1] => -49
                                                                    [2] => SO
                                                                    [3] => 299
                                                                    [4] => 1432564660
                                                                    [5] => resourceWalker
                                                                )

                                                            [2] => Array
                                                                (
                                                                    [0] => 163
                                                                    [1] => 13
                                                                    [2] => SO
                                                                    [3] => 299
                                                                    [4] => 1432564665
                                                                    [5] => resourceWalker
                                                                )

                                                            [3] => Array
                                                                (
                                                                    [0] => 241
                                                                    [1] => 59
                                                                    [2] => SO
                                                                    [3] => 299
                                                                    [4] => 1432564671
                                                                    [5] => resourceWalker
                                                                )

                                                            [4] => Array
                                                                (
                                                                    [0] => 267
                                                                    [1] => 105
                                                                    [2] => SO
                                                                    [3] => 299
                                                                    [4] => 1432564675
                                                                    [5] => resourceWalker
                                                                )

                                                            [5] => Array
                                                                (
                                                                    [0] => 314
                                                                    [1] => 128
                                                                    [2] => SO
                                                                    [3] => 299
                                                                    [4] => 1432564678
                                                                    [5] => resourceWalker
                                                                )

                                                            [6] => Array
                                                                (
                                                                    [0] => 407
                                                                    [1] => 183
                                                                    [2] => SO
                                                                    [3] => 299
                                                                    [4] => 1432564686
                                                                    [5] => resourceWalker
                                                                )

                                                            [7] => Array
                                                                (
                                                                    [0] => 430
                                                                    [1] => 230
                                                                    [2] => SO
                                                                    [3] => 299
                                                                    [4] => 1432564689
                                                                    [5] => resourceWalker
                                                                )

                                                            [8] => Array
                                                                (
                                                                    [0] => 487
                                                                    [1] => 261
                                                                    [2] => SO
                                                                    [3] => 299
                                                                    [4] => 1432564693
                                                                    [5] => resourceWalker
                                                                )

                                                            [9] => Array
                                                                (
                                                                    [0] => 514
                                                                    [1] => 276
                                                                    [2] => SO
                                                                    [3] => 310
                                                                    [4] => 1432564696
                                                                    [5] => resourceWalker
                                                                )

                                                            [10] => Array
                                                                (
                                                                    [0] => 540
                                                                    [1] => 290
                                                                    [2] => SO
                                                                    [3] => 310
                                                                    [4] => 1432564697
                                                                    [5] => resourceWalker
                                                                )

                                                            [11] => Array
                                                                (
                                                                    [0] => 548
                                                                    [1] => 323
                                                                    [2] => S
                                                                    [3] => 299
                                                                    [4] => 1432564700
                                                                    [5] => resourceWalker
                                                                )

                                                            [12] => Array
                                                                (
                                                                    [0] => 777
                                                                    [1] => 444
                                                                    [2] => SO
                                                                    [3] => 299
                                                                    [4] => 1432564717
                                                                    [5] => resourceWalker
                                                                )

                                                            [13] => Array
                                                                (
                                                                    [0] => 857
                                                                    [1] => 462
                                                                    [2] => O
                                                                    [3] => 310
                                                                    [4] => 1432564723
                                                                    [5] => resourceWalker
                                                                )

                                                            [14] => Array
                                                                (
                                                                    [0] => 777
                                                                    [1] => 444
                                                                    [2] => W
                                                                    [3] => 310
                                                                    [4] => 1432564728
                                                                    [5] => resourceWalkerEmpty
                                                                )

                                                            [15] => Array
                                                                (
                                                                    [0] => 548
                                                                    [1] => 323
                                                                    [2] => NW
                                                                    [3] => 299
                                                                    [4] => 1432564745
                                                                    [5] => resourceWalkerEmpty
                                                                )

                                                            [16] => Array
                                                                (
                                                                    [0] => 540
                                                                    [1] => 290
                                                                    [2] => N
                                                                    [3] => 299
                                                                    [4] => 1432564748
                                                                    [5] => resourceWalkerEmpty
                                                                )

                                                            [17] => Array
                                                                (
                                                                    [0] => 514
                                                                    [1] => 276
                                                                    [2] => NW
                                                                    [3] => 310
                                                                    [4] => 1432564749
                                                                    [5] => resourceWalkerEmpty
                                                                )

                                                            [18] => Array
                                                                (
                                                                    [0] => 487
                                                                    [1] => 261
                                                                    [2] => NW
                                                                    [3] => 310
                                                                    [4] => 1432564752
                                                                    [5] => resourceWalkerEmpty
                                                                )

                                                            [19] => Array
                                                                (
                                                                    [0] => 430
                                                                    [1] => 230
                                                                    [2] => NW
                                                                    [3] => 299
                                                                    [4] => 1432564756
                                                                    [5] => resourceWalkerEmpty
                                                                )

                                                            [20] => Array
                                                                (
                                                                    [0] => 407
                                                                    [1] => 183
                                                                    [2] => NW
                                                                    [3] => 299
                                                                    [4] => 1432564759
                                                                    [5] => resourceWalkerEmpty
                                                                )

                                                            [21] => Array
                                                                (
                                                                    [0] => 314
                                                                    [1] => 128
                                                                    [2] => NW
                                                                    [3] => 299
                                                                    [4] => 1432564767
                                                                    [5] => resourceWalkerEmpty
                                                                )

                                                            [22] => Array
                                                                (
                                                                    [0] => 267
                                                                    [1] => 105
                                                                    [2] => NW
                                                                    [3] => 299
                                                                    [4] => 1432564770
                                                                    [5] => resourceWalkerEmpty
                                                                )

                                                            [23] => Array
                                                                (
                                                                    [0] => 241
                                                                    [1] => 59
                                                                    [2] => NW
                                                                    [3] => 299
                                                                    [4] => 1432564774
                                                                    [5] => resourceWalkerEmpty
                                                                )

                                                            [24] => Array
                                                                (
                                                                    [0] => 163
                                                                    [1] => 13
                                                                    [2] => NW
                                                                    [3] => 299
                                                                    [4] => 1432564780
                                                                    [5] => resourceWalkerEmpty
                                                                )

                                                            [25] => Array
                                                                (
                                                                    [0] => 112
                                                                    [1] => -49
                                                                    [2] => NW
                                                                    [3] => 299
                                                                    [4] => 1432564785
                                                                    [5] => resourceWalkerEmpty
                                                                )

                                                            [26] => Array
                                                                (
                                                                    [0] => 112
                                                                    [1] => -49
                                                                    [2] => none
                                                                    [3] => 299
                                                                    [4] => 1432564867
                                                                    [5] => resourceWalkerEmpty
                                                                )

                                                            [27] => Array
                                                                (
                                                                    [0] => 112
                                                                    [1] => -49
                                                                    [2] => none
                                                                    [3] => 299
                                                                    [4] => 1432564881
                                                                    [5] => resourceWalkerEmpty
                                                                )

                                                        )

                                                    [3] => 1432564867
                                                    [4] => Co vlastně starosta se všemi těmito surovinami dělá?<br>Produkce: <img src="skin/resources/icon_wood_small.png" /> +200 za Hodinu
                                                    [5] => 9.2233720368548E+18
                                                    [6] => 0
                                                )

                                        )

                                )

                        )

                    [nextETA] => 0
                    [friends] => {"1":{"friendId":"45390","position":"1","newPlayer":"0","name":"budovatelpepa","ally_tag":"-DVG-","ally_id":"1666","inactive":"0","vacation_mode":"0","gender":"0","lastclick":"1432522240","activity_state":2,"banned":false}}
                    [removeSideBarExt] => 0
                )

        )

    [1] => Array
        (
            [0] => updateTemplateData
            [1] => Array
                (
                )

        )

    [2] => Array
        (
            [0] => provideFeedback
            [1] => Array
                (
                    [0] => Array
                        (
                            [location] => 5
                            [text] => Vaše lodě jsou již v vašem přístavu!
                            [type] => 11
                        )

                )

        )

    [3] => Array
        (
            [0] => questData
            [1] => Array
                (
                    [showReward] => 
                    [hideTutorialAdvisor] => 1
                )

        )

    [4] => Array
        (
            [0] => popupData
            [1] => 
        )

    [5] => Array
        (
            [0] => ingameCounterData
            [1] => 
        )

    [6] => Array
        (
            [0] => removeIngameCounterData
            [1] => 
        )

    [7] => Array
        (
            [0] => updateBacklink
            [1] => 
        )

)
